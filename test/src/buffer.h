#ifndef INCLUDE_test_buffer_h__
#define INCLUDE_test_buffer_h__

#include "test.h"

DECL_TEST(nix_buffer__construct);
DECL_TEST(nix_buffer__init);
DECL_TEST(nix_buffer__init_utf16);
DECL_TEST(__read_bom_utf16);
DECL_TEST(__read_bom_utf16_low_endian);
DECL_TEST(__read_bom_utf8);
DECL_TEST(__read_bom_none);
DECL_TEST(__read_bom_eof);
DECL_TEST(__read_bom_byte);
DECL_TEST(nix_buffer__construct_bytes);
DECL_TEST(nix_buffer__init_bytes);
DECL_TEST(nix_buffer__init_bytes_utf16);
DECL_TEST(__read_bom_from_buffer);
DECL_TEST(nix_buffer__read);
DECL_TEST(nix_buffer__read_lines);
DECL_TEST(nix_buffer__read_exhaust_reset);
DECL_TEST(nix_buffer__peek);
DECL_TEST(nix_buffer__peek_exhaust);
DECL_TEST(__read);
DECL_TEST(__read_lines);
DECL_TEST(__read_utf8_ascii);
DECL_TEST(__read_utf8_multibyte);
DECL_TEST(__count_utf8_encoded_bytes);
DECL_TEST(__read_utf16_BE_no_surrogates);
DECL_TEST(__read_utf16_LE_no_surrogates);
DECL_TEST(__read_utf16_BE_surrogates);
DECL_TEST(__read_utf16_LE_surrogates);
DECL_TEST(__read_utf16_data_BE);
DECL_TEST(__read_utf16_data_LE);
DECL_TEST(__read_byte);
DECL_TEST(__check_bounds);
DECL_TEST(nix_buffer__reset_peek);
DECL_TEST(__buffer_side);
DECL_TEST(__load_buffer);
DECL_TEST(__buffer_occupied);
DECL_TEST(__increment);
DECL_TEST(nix_buffer__read_lexeme);

#endif
