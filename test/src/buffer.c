#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "libnix/error.h"
#include "libnix/buffer.h"
#include "src/buffer.h"
#include "buffer.h"
#include "test.h"

int main() {
    TESTS_START

    RUN_TEST(nix_buffer__construct, "Buffer is constructed");
    RUN_TEST(nix_buffer__init, "Buffer is initiated");
    RUN_TEST(nix_buffer__init_utf16, "Buffer initiated with UTF-16 data");
    RUN_TEST(__read_bom_utf16, "Detects high-endian UTF-16");
    RUN_TEST(__read_bom_utf16_low_endian, "Detects low-endian UTF-16");
    RUN_TEST(__read_bom_utf8, "Detects (unnecessary) UTF-8 BOM");
    RUN_TEST(__read_bom_none, "No false positives on BOM");
    RUN_TEST(__read_bom_eof, "No false positives for BOM on EOF");
    RUN_TEST(__read_bom_byte, "Reading BOM byte from file");
    RUN_TEST(nix_buffer__construct_bytes, "Buffer constructed from string");
    RUN_TEST(nix_buffer__init_bytes, "Buffer initialized from string");
    RUN_TEST(nix_buffer__init_bytes_utf16, "Buffer with UTF-16 string");
    RUN_TEST(__read_bom_from_buffer, "Detect BOM from string buffer");
    RUN_TEST(nix_buffer__read, "Can read bytes from buffer");
    RUN_TEST(nix_buffer__read_lines, "Lexeme tracks lines and columns");
    RUN_TEST(nix_buffer__read_exhaust_reset, "Position reset when exhausted");
    RUN_TEST(nix_buffer__peek, "Can peek bytes from buffer");
    RUN_TEST(nix_buffer__peek_exhaust, "Exhaust buffer by peeking");
    RUN_TEST(__read, "Can read characters from buffers");
    RUN_TEST(__read_lines, "Can track lines and columns");
    RUN_TEST(__read_utf8_ascii, "Read UTF-8 ASCII bytes");
    RUN_TEST(__read_utf8_multibyte, "Read multi-byte UTF-8 characters");
    RUN_TEST(__count_utf8_encoded_bytes, "Find byte-count from first byte");
    RUN_TEST(__read_utf16_BE_no_surrogates, "Read simple BE UTF-16 values");
    RUN_TEST(__read_utf16_LE_no_surrogates, "Read simple LE UTF-16 values");
    RUN_TEST(__read_utf16_BE_surrogates, "Read complex BE UTF-16 values");
    RUN_TEST(__read_utf16_LE_surrogates, "Read complex LE UTF-16 values");
    RUN_TEST(__read_utf16_data_BE, "Read 16-bit values from from BE UTF-16");
    RUN_TEST(__read_utf16_data_LE, "Read 16-bit values from from LE UTF-16");
    RUN_TEST(__read_byte, "Read byte from buffer (with/without bounds check)");
    RUN_TEST(__check_bounds, "Check buffer boundaries");
    RUN_TEST(nix_buffer__reset_peek, "Reset peek pointer");
    RUN_TEST(__buffer_side, "Detect buffer side of pointer");
    RUN_TEST(__load_buffer, "Load data from file into buffer");
    RUN_TEST(__buffer_occupied, "Check when buffers are occupied");
    RUN_TEST(__increment, "Increment a pointer through the buffer");
    RUN_TEST(nix_buffer__read_lexeme, "Read a lexeme");

    TESTS_FINISH
}

TEST(nix_buffer__construct) {
    TEST_START

    FILE *input = NULL;
    FILE_FROM_STRING(input, (uint8_t *)"", 0);

    struct nix_buffer *b = NULL;
    enum nix_err result = nix_buffer__construct(&b, input, 16);

    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_NNULL(b);

    TEST_FINISH(success);
    CLOSE_FILE(input);
    nix_buffer__free(&b);

    return success;
}

TEST(nix_buffer__init) {
    TEST_START

    FILE *input = NULL;
    FILE_FROM_STRING(input, (uint8_t *)"", 0);

    struct buffer *b = malloc(sizeof(struct buffer));
    ASSERT_NNULL(b);
    memset(b, 0, sizeof(struct buffer));

    enum nix_err result = nix_buffer__init(b, input, 0x10);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_NNULL(b->buffer);
    ASSERT_EQ(b->left, b->buffer);
    ASSERT_EQ(b->right, b->buffer + 0x10);
    ASSERT_EQ(b->input, input);
    ASSERT_EQ(b->reader, &__read_utf8);
    ASSERT_FALSE(b->utf16);
    ASSERT_FALSE(b->reverse_order);
    ASSERT_TRUE(b->buffer_ready[BUFFER_LEFT]);
    ASSERT_FALSE(b->buffer_ready[BUFFER_RIGHT]);
    ASSERT_FALSE(b->at_eof);
    ASSERT_EQ(b->eof, b->buffer);
    ASSERT_EQ(b->lexeme_start, b->buffer);
    ASSERT_EQ(b->read, b->buffer);
    ASSERT_EQ(b->peek, b->buffer);

    struct nix_buffer *p = &b->p;

    ASSERT_EQ(p->buffer_size, 0x10);
    ASSERT_NNULL(p->read);
    ASSERT_NNULL(p->peek);
    ASSERT_NNULL(p->lexeme_start);
    ASSERT_NNULL(p->lexeme_end);


    TEST_FINISH(success);
    CLOSE_FILE(input);
    nix_buffer__free((struct nix_buffer **)&b);

    return success;
}

TEST(nix_buffer__init_utf16) {
    TEST_START

    FILE *input = NULL;
    FILE_FROM_STRING(input, (uint8_t *)"\xFE\xFF", 2);

    struct buffer *b = malloc(sizeof(struct buffer));
    ASSERT_NNULL(b)
    memset(b, 0, sizeof(struct buffer));

    enum nix_err result = nix_buffer__init(b, input, 0x10);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(b->reader, &__read_utf16);

    TEST_FINISH(success);
    CLOSE_FILE(input);
    nix_buffer__free((struct nix_buffer **)&b);

    return success;
}

TEST(__read_bom_utf16) {
    TEST_START

    FILE *input = NULL;
    FILE_FROM_STRING(input, (uint8_t *)"\xFE\xFF", 2);

    bool utf16 = false;
    bool reverse_order = true;
    enum nix_err result = __read_bom(input, &utf16, &reverse_order);

    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_TRUE(utf16);
    ASSERT_FALSE(reverse_order);

    TEST_FINISH(success);
    CLOSE_FILE(input);

    return success;
}

TEST(__read_bom_utf16_low_endian) {
    TEST_START

    FILE *input = NULL;
    FILE_FROM_STRING(input, (uint8_t *)"\xFF\xFE", 2);

    bool utf16 = false;
    bool reverse_order = false;
    enum nix_err result = __read_bom(input, &utf16, &reverse_order);

    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_TRUE(utf16);
    ASSERT_TRUE(reverse_order);

    TEST_FINISH(success);
    CLOSE_FILE(input);

    return success;
}

TEST(__read_bom_utf8) {
    TEST_START

    FILE *input = NULL;
    FILE_FROM_STRING(input, (uint8_t *)"\xEF\xBB\xBF", 3);

    bool utf16 = true;
    bool reverse_order = true;
    enum nix_err result = __read_bom(input, &utf16, &reverse_order);

    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_FALSE(utf16);
    ASSERT_FALSE(reverse_order);

    TEST_FINISH(success);
    CLOSE_FILE(input);

    return success;
}

TEST(__read_bom_none) {
    TEST_START

    FILE *input = NULL;
    enum nix_err result;
    bool utf16 = true;
    bool reverse_order = true;

    FILE_FROM_STRING(input, (uint8_t *)"abcd", 4);
    result = __read_bom(input, &utf16, &reverse_order);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_FALSE(utf16);
    ASSERT_FALSE(reverse_order);

    FILE_FROM_STRING(input, (uint8_t *)"\xFF\x41\x42\x43", 4);
    result = __read_bom(input, &utf16, &reverse_order);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_FALSE(utf16);
    ASSERT_FALSE(reverse_order);

    FILE_FROM_STRING(input, (uint8_t *)"\xFE\xFE\x41\x42", 4)
    result = __read_bom(input, &utf16, &reverse_order);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_FALSE(utf16);
    ASSERT_FALSE(reverse_order);

    TEST_FINISH(success);
    CLOSE_FILE(input);

    return success;
}

TEST(__read_bom_eof) {
    TEST_START

    FILE *input = NULL;
    enum nix_err result;
    bool utf16 = true;
    bool reverse_order = true;

    FILE_FROM_STRING(input, (uint8_t *)"\xFE", 1);
    result = __read_bom(input, &utf16, &reverse_order);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_FALSE(utf16);
    ASSERT_FALSE(reverse_order);

    FILE_FROM_STRING(input, (uint8_t *)"\xFF", 1);
    result = __read_bom(input, &utf16, &reverse_order);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_FALSE(utf16);
    ASSERT_FALSE(reverse_order);

    FILE_FROM_STRING(input, (uint8_t *)"\xEF\xBB", 2);
    result = __read_bom(input, &utf16, &reverse_order);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_FALSE(utf16);
    ASSERT_FALSE(reverse_order);

    TEST_FINISH(success);
    CLOSE_FILE(input);

    return success;
}

TEST(__read_bom_byte) {
    TEST_START

    FILE *input = NULL;
    enum nix_err result;
    uint8_t byte;

    FILE_FROM_STRING(input, (uint8_t *)"\x41", 1);
    result = __read_bom_byte(input, &byte);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(byte, 0x41);

    result = __read_bom_byte(input, &byte);
    ASSERT_EQ(result, NIXERR_BUF_EOF);

    fclose(input);
    result = __read_bom_byte(input, &byte);
    input = NULL;
    ASSERT_EQ(result, NIXERR_BUF_FILE);

    TEST_FINISH(success);
    CLOSE_FILE(input);

    return success;
}

TEST(nix_buffer__construct_bytes) {
    TEST_START

    struct nix_buffer *b = NULL;
    enum nix_err result = nix_buffer__construct_bytes(&b, (uint8_t *)"abc", 3);

    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_NNULL(b);

    TEST_FINISH(success);
    nix_buffer__free(&b);

    return success;
}

TEST(nix_buffer__init_bytes) {
    TEST_START

    struct buffer *b = malloc(sizeof(struct buffer));
    ASSERT_NNULL(b);
    memset(b, 0, sizeof(struct buffer));

    enum nix_err result = nix_buffer__init_bytes(b, (uint8_t *)"abc", 3);
    ASSERT_EQ(result, NIXERR_NONE);

    ASSERT_NNULL(b->buffer);
    ASSERT_EQ(b->left, b->buffer);
    ASSERT_EQ(b->right, b->buffer + 0x2);
    ASSERT_NULL_M(b->input, "String buffer has no input file");
    ASSERT_EQ(*b->left, 'a');
    ASSERT_EQ(*b->right, 'c');
    ASSERT_FALSE(b->utf16);
    ASSERT_FALSE(b->reverse_order);
    ASSERT_EQ(b->reader, &__read_utf8);
    ASSERT_TRUE(b->buffer_ready[BUFFER_LEFT]);
    ASSERT_TRUE(b->buffer_ready[BUFFER_RIGHT]);
    ASSERT_EQ(b->read, b->left);
    ASSERT_EQ(b->peek, b->left);
    ASSERT_EQ(b->lexeme_start, b->left);

    struct nix_buffer *p = (struct nix_buffer *)b;

    ASSERT_EQ(p->buffer_size, 2);
    ASSERT_NNULL(p->read);
    ASSERT_NNULL(p->peek);
    ASSERT_NNULL(p->lexeme_start);
    ASSERT_NNULL(p->lexeme_end);

    TEST_FINISH(success);
    nix_buffer__free((struct nix_buffer **)&b);

    return success;
}

TEST(nix_buffer__init_bytes_utf16) {
    TEST_START

    struct buffer *b = malloc(sizeof(struct buffer));
    ASSERT_NNULL(b);
    memset(b, 0, sizeof(struct buffer));

    enum nix_err result = nix_buffer__init_bytes(b, (uint8_t *)"\xFE\xFF", 2);
    ASSERT_EQ(result, NIXERR_NONE);

    ASSERT_TRUE(b->utf16);
    ASSERT_EQ(b->reader, &__read_utf16);
    ASSERT_EQ(b->read, b->left + 0x2);
    ASSERT_EQ(b->peek, b->read);
    ASSERT_EQ(b->lexeme_start, b->read);

    TEST_FINISH(success);
    nix_buffer__free((struct nix_buffer **)&b);

    return success;
}

TEST(__read_bom_from_buffer) {
    TEST_START

    bool utf16;
    bool reverse;
    size_t read;

    uint8_t *data = (uint8_t *)"\x41\x42";
    __read_bom_from_buffer(data, 2, &utf16, &reverse, &read);
    ASSERT_FALSE(utf16);
    ASSERT_FALSE(reverse);
    ASSERT_EQ(read, 0);

    data = (uint8_t *)"\xFE\xFF";
    __read_bom_from_buffer(data, 2, &utf16, &reverse, &read);
    ASSERT_TRUE(utf16);
    ASSERT_FALSE(reverse);
    ASSERT_EQ(read, 2);

    data = (uint8_t *)"\xFF\xFE";
    __read_bom_from_buffer(data, 2, &utf16, &reverse, &read);
    ASSERT_TRUE(utf16);
    ASSERT_TRUE(reverse);
    ASSERT_EQ(read, 2);

    data = (uint8_t *)"\xEF\xBB\xBF";
    __read_bom_from_buffer(data, 3, &utf16, &reverse, &read);
    ASSERT_FALSE(utf16);
    ASSERT_FALSE(reverse);
    ASSERT_EQ(read, 3);

    TEST_FINISH(success);
    return success;
}

TEST(nix_buffer__read) {
    TEST_START

    struct nix_buffer *b = NULL;
    uint8_t *data = (uint8_t *)"abcd";
    enum nix_err result = nix_buffer__construct_bytes(&b, data, 4);

    ASSERT_NNULL(b);
    ASSERT_EQ(result, NIXERR_NONE);

    uint32_t c;
    result = nix_buffer__read(b, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 'a');

    result = nix_buffer__read(b, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 'b');

    result = nix_buffer__read(b, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 'c');

    result = nix_buffer__read(b, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 'd');

    result = nix_buffer__read(b, &c);
    ASSERT_EQ(result, NIXERR_BUF_EOF);

    result = nix_buffer__read(b, &c);
    ASSERT_EQ(result, NIXERR_BUF_PAST_EOF);

    TEST_FINISH(success)
    nix_buffer__free(&b);

    return success;
}

TEST(nix_buffer__read_lines) {
    TEST_START

    struct nix_buffer *p = NULL;
    uint8_t *data = (uint8_t *)"\n\r\n\r\r\n";
    enum nix_err result = nix_buffer__construct_bytes(&p, data, 6);

    ASSERT_NNULL(data);
    ASSERT_EQ(result, NIXERR_NONE);

    struct buffer *b = (struct buffer *)p;

    uint32_t c;
    result = nix_buffer__read(p, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, '\n');
    ASSERT_EQ(b->lexeme_end, b->left);
    ASSERT_POS_EQ(p->lexeme_end, 1, 1, 0);

    result = nix_buffer__read(p, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, '\r');
    ASSERT_EQ(b->lexeme_end, b->left + 0x1);
    ASSERT_POS_EQ(p->lexeme_end, 2, 1, 1);

    result = nix_buffer__read(p, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, '\n');
    ASSERT_EQ(b->lexeme_end, b->left + 0x2);
    ASSERT_POS_EQ(p->lexeme_end, 2, 2, 2);

    result = nix_buffer__read(p, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, '\r');
    ASSERT_EQ(b->lexeme_end, b->left + 0x3);
    ASSERT_POS_EQ(p->lexeme_end, 3, 1, 3);

    result = nix_buffer__read(p, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, '\r');
    ASSERT_EQ(b->lexeme_end, b->left + 0x4);
    ASSERT_POS_EQ(p->lexeme_end, 4, 1, 4);

    result = nix_buffer__read(p, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, '\n');
    ASSERT_EQ(b->lexeme_end, b->left + 0x5);
    ASSERT_POS_EQ(p->lexeme_end, 4, 2, 5);

    TEST_FINISH(success)
    return success;
}

TEST(nix_buffer__read_exhaust_reset) {
    TEST_START

    FILE *input = NULL;
    FILE_FROM_STRING(input, (uint8_t *)"\xC3\xBF\xC3\xBE\x41", 5);

    struct nix_buffer *p = NULL;
    enum nix_err result = nix_buffer__construct(&p, input, 2);
    ASSERT_NNULL(p);
    ASSERT_EQ(result, NIXERR_NONE);

    struct buffer *b = (struct buffer *)p;

    uint32_t c;
    result = nix_buffer__read(p, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0xFF);
    ASSERT_EQ(p->read->abs, 1);
    ASSERT_EQ(b->read, b->left + 0x2);
    ASSERT_EQ(b->peek, b->read);
    ASSERT_EQ(b->lexeme_start, b->left);
    ASSERT_EQ(b->lexeme_end, b->left);

    result = nix_buffer__read(p, &c);
    ASSERT_EQ(result, NIXERR_BUF_EXHAUST);
    ASSERT_EQ(p->read->abs, 1);
    ASSERT_EQ(b->read, b->left + 0x2);
    ASSERT_EQ(b->peek, b->read);
    ASSERT_EQ(b->lexeme_start, b->left);
    ASSERT_EQ(b->lexeme_end, b->left);

    result = nix_buffer__discard_lexeme(p, 0);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(b->lexeme_end, NULL);

    result = nix_buffer__read(p, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0xFE);
    ASSERT_EQ(p->read->abs, 2);
    ASSERT_EQ(b->read, b->left);
    ASSERT_EQ(b->peek, b->read);
    ASSERT_EQ(b->lexeme_start, b->right);
    ASSERT_EQ(b->lexeme_end, b->right);

    result = nix_buffer__read(p, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, '\x41');
    ASSERT_EQ(p->read->abs, 3);
    ASSERT_EQ(b->read, b->left + 0x1);
    ASSERT_EQ(b->peek, b->read);
    ASSERT_EQ(b->lexeme_start, b->right);

    result = nix_buffer__read(p, &c);
    ASSERT_EQ(result, NIXERR_BUF_EOF);

    result = nix_buffer__read(p, &c);
    ASSERT_EQ(result, NIXERR_BUF_PAST_EOF);

    TEST_FINISH(success)
    CLOSE_FILE(input);
    nix_buffer__free(&p);

    return success;
}

TEST(nix_buffer__peek) {
    TEST_START

    struct nix_buffer *p = NULL;
    uint8_t *data = (uint8_t *)"abcd";
    enum nix_err result = nix_buffer__construct_bytes(&p, data, 4);

    ASSERT_NNULL(p);
    ASSERT_EQ(result, NIXERR_NONE);

    struct buffer *b = (struct buffer *)p;

    uint32_t c;
    result = nix_buffer__peek(p, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 'a');
    ASSERT_EQ(b->read, b->left);
    ASSERT_EQ(b->peek, b->read + 0x1);

    result = nix_buffer__peek(p, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 'b');
    ASSERT_EQ(b->read, b->left);
    ASSERT_EQ(b->peek, b->read + 0x2);

    result = nix_buffer__read(p, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 'a');
    ASSERT_EQ(b->read, b->left + 0x1);
    ASSERT_EQ(b->peek, b->read);

    ASSERT_EQ(b->lexeme_start, b->left);

    TEST_FINISH(success)
    nix_buffer__free(&p);

    return success;
}

TEST(nix_buffer__peek_exhaust) {
    TEST_START

    FILE *input = NULL;
    FILE_FROM_STRING(input, (uint8_t *)"\xC3\xBF\xC3\xBE\x41", 5);

    struct nix_buffer *p = NULL;
    enum nix_err result = nix_buffer__construct(&p, input, 2);
    ASSERT_NNULL(p);
    ASSERT_EQ(result, NIXERR_NONE);

    struct buffer *b = (struct buffer *)p;

    uint32_t c;
    result = nix_buffer__peek(p, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0xFF);

    result = nix_buffer__peek(p, &c);
    ASSERT_EQ(result, NIXERR_BUF_EXHAUST);

    result = nix_buffer__read(p, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0xFF);

    result = nix_buffer__peek(p, &c);
    ASSERT_EQ(result, NIXERR_BUF_EXHAUST);

    result = nix_buffer__discard_lexeme(p, 0);
    ASSERT_EQ(result, NIXERR_NONE);

    result = nix_buffer__peek(p, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0xFE);

    TEST_FINISH(success)
    CLOSE_FILE(input);
    nix_buffer__free(&p);

    return success;
}

TEST(__read) {
    TEST_START

    struct buffer *b = malloc(sizeof(struct buffer));
    ASSERT_NNULL(b);
    memset(b, 0, sizeof(struct buffer));

    enum nix_err result = nix_buffer__init_bytes(b, (uint8_t *)"a", 1);
    ASSERT_EQ(result, NIXERR_NONE);

    uint32_t c;
    result = __read(b, &c, &b->read, b->p.read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 'a');
    ASSERT_EQ(b->read, b->left + 0x1);
    ASSERT_EQ(b->p.read->row, 1);
    ASSERT_EQ(b->p.read->col, 2);

    TEST_FINISH(success)
    return success;
}

TEST(__read_lines) {
    TEST_START

    struct buffer *b = malloc(sizeof(struct buffer));
    ASSERT_NNULL(b);
    memset(b, 0, sizeof(struct buffer));

    uint8_t *data = (uint8_t *)"a\nb\r\nc\rd\n\r";
    enum nix_err result = nix_buffer__init_bytes(b, data, 10);
    ASSERT_EQ(result, NIXERR_NONE);

    uint32_t c;
    ASSERT_POS_EQ(b->p.read, 1, 1, 0);
    ASSERT_EQ(b->lexeme_start, b->read);
    ASSERT_NULL(b->lexeme_end);

    result = __read(b, &c, &b->read, b->p.read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 'a');
    ASSERT_POS_EQ(b->p.read, 1, 2, 1);

    result = __read(b, &c, &b->read, b->p.read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, '\n');
    ASSERT_POS_EQ(b->p.read, 2, 1, 2);

    result = __read(b, &c, &b->read, b->p.read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 'b');
    ASSERT_POS_EQ(b->p.read, 2, 2, 3);

    result = __read(b, &c, &b->read, b->p.read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, '\r');
    ASSERT_POS_EQ(b->p.read, 2, 3, 4);

    result = __read(b, &c, &b->read, b->p.read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, '\n');
    ASSERT_POS_EQ(b->p.read, 3, 1, 5);

    result = __read(b, &c, &b->read, b->p.read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 'c');
    ASSERT_POS_EQ(b->p.read, 3, 2, 6);

    result = __read(b, &c, &b->read, b->p.read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, '\r');
    ASSERT_POS_EQ(b->p.read, 4, 1, 7);

    TEST_FINISH(success)
    return success;
}

TEST(__read_utf8_ascii) {
    TEST_START

    struct nix_buffer *p = NULL;
    uint8_t *data = (uint8_t *)"\x41\x7F\x80";
    nix_buffer__construct_bytes(&p, data, 3);

    struct buffer *b = (struct buffer *)p;
    uint32_t c;

    __read_utf8(b, &c, &b->read, false);
    ASSERT_EQ(c, 0x41);

    __read_utf8(b, &c, &b->read, false);
    ASSERT_EQ(c, 0x7F);

    enum nix_err result = __read_utf8(b, &c, &b->read, false);
    ASSERT_EQ(result, NIXERR_BUF_INVCHAR);
    ASSERT_EQ_M(c, 0x80, "Still return byte if it's invalid UTF-8");

    result = __read_utf8(b, &c, &b->read, true);
    ASSERT_EQ_M(result, NIXERR_BUF_EOF,
        "Pointer advanced beyond invalid UTF-8 characters");

    TEST_FINISH(success)
    return success;
}

TEST(__read_utf8_multibyte) {
    TEST_START

    uint8_t data[] = {
        0xC3, 0xA5, // å, U+E5
        0xCE, 0xA6, // Φ, U+03A6
        0xF0, 0x9F, 0x98, 0x95 // Emoji, U+1F615
    };

    struct nix_buffer *p = NULL;
    enum nix_err result = nix_buffer__construct_bytes(&p, (uint8_t *)data, 8);
    ASSERT_NNULL(p);
    ASSERT_EQ(result, NIXERR_NONE);

    struct buffer *b = (struct buffer *)p;

    uint32_t c;
    result = __read_utf8(b, &c, &b->read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0xE5);

    result = __read_utf8(b, &c, &b->read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0x03A6);

    result = __read_utf8(b, &c, &b->read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0x1F615);

    TEST_FINISH(success)
    nix_buffer__free(&p);

    return success;
}

TEST(__count_utf8_encoded_bytes) {
    TEST_START

    enum nix_err result;
    size_t count;

    // 01111111 (0x7F) - ASCII byte (invalid)
    result = __count_utf8_encoded_bytes(0x7F, &count);
    ASSERT_EQ_M(result, NIXERR_BUF_INVCHAR, "ASCII bytes invalid");

    // 10111111 (0xBF) - intermediate byte (invalid)
    result = __count_utf8_encoded_bytes(0x80, &count);
    ASSERT_EQ_M(result, NIXERR_BUF_INVCHAR, "Intermediate bytes invalid");

    // 11011111 (0xDF) - 2 bytes
    result = __count_utf8_encoded_bytes(0xDF, &count);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(count, 2);

    // 11101111 (0xEF) - 3 bytes
    result = __count_utf8_encoded_bytes(0xEF, &count);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(count, 3);

    // 11110111 (0xF7) - 4 bytes
    result = __count_utf8_encoded_bytes(0xF7, &count);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(count, 4);

    // 11111111 (0xFF) - no 0 delimiter (invalid)
    result = __count_utf8_encoded_bytes(0xFF, &count);
    ASSERT_EQ(result, NIXERR_BUF_INVCHAR);

    TEST_FINISH(success)
    return success;
}

TEST(__read_utf16_BE_no_surrogates) {
    TEST_START

    struct buffer *b = malloc(sizeof(struct buffer));
    ASSERT_NNULL(b);
    memset(b, 0, sizeof(struct buffer));

    uint8_t data[] = {
        0xFE, 0xFF, // UTF-16 BE BOM
        0x10, 0x50, // ၐ, U+1050 (below surrogate region)
        0xF9, 0x2B  // 狼, U+F92B (above surrogate region)
    };
    enum nix_err result = nix_buffer__init_bytes(b, data, 6);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_TRUE(b->utf16);
    ASSERT_FALSE(b->reverse_order);
    ASSERT_EQ(b->read, b->left + 0x2);

    uint32_t c;

    result = __read_utf16(b, &c, &b->read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0x1050);

    result = __read_utf16(b, &c, &b->read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0xF92B);

    TEST_FINISH(success)
    nix_buffer__free((struct nix_buffer **)&b);

    return success;
}

TEST(__read_utf16_LE_no_surrogates) {
    TEST_START

    struct buffer *b = malloc(sizeof(struct buffer));
    ASSERT_NNULL(b);
    memset(b, 0, sizeof(struct buffer));

    uint8_t data[] = {
        0xFF, 0xFE, // UTF-16 LE BOM
        0x50, 0x10, // ၐ, U+1050 (below surrogate region)
        0x2B, 0xF9  // 狼, U+F92B (above surrogate region)
    };
    enum nix_err result = nix_buffer__init_bytes(b, data, 6);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_TRUE(b->utf16);
    ASSERT_TRUE(b->reverse_order);
    ASSERT_EQ(b->read, b->left + 0x2);

    uint32_t c;

    result = __read_utf16(b, &c, &b->read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0x1050);

    result = __read_utf16(b, &c, &b->read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0xF92B);

    TEST_FINISH(success)
    nix_buffer__free((struct nix_buffer **)&b);

    return success;
}

TEST(__read_utf16_BE_surrogates) {
    TEST_START

    uint8_t data[] = {
        0xFE, 0xFF, // UTF-16 BE BOM
        0xD8, 0x01, 0xDC, 0x00, // 𐐀, U+10400
        0xD8, 0x3D, 0xDE, 0x16, // 😖, U+1F616
        0xDB, 0xFF, 0xDF, 0xFF  // highest code point, U+10FFFF
    };

    struct nix_buffer *p = NULL;
    enum nix_err result = nix_buffer__construct_bytes(&p, data, 14);
    ASSERT_EQ(result, NIXERR_NONE);

    struct buffer *b = (struct buffer *)p;
    uint32_t c;

    result = __read_utf16(b, &c, &b->read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0x10400);

    result = __read_utf16(b, &c, &b->read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0x1F616);

    result = __read_utf16(b, &c, &b->read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0x10FFFF);

    TEST_FINISH(success)
    nix_buffer__free(&p);

    return success;
}

TEST(__read_utf16_LE_surrogates) {
    TEST_START

    uint8_t data[] = {
        0xFF, 0xFE, // UTF-16 BE BOM
        0x01, 0xD8, 0x00, 0xDC, // 𐐀, U+10400
        0x3D, 0xD8, 0x16, 0xDE, // 😖, U+1F616
        0xFF, 0xDB, 0xFF, 0xDF  // noncharacter, U+10FFFF
    };

    struct nix_buffer *p = NULL;
    enum nix_err result = nix_buffer__construct_bytes(&p, data, 14);
    ASSERT_EQ(result, NIXERR_NONE);

    struct buffer *b = (struct buffer *)p;
    uint32_t c;

    result = __read_utf16(b, &c, &b->read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0x10400);

    result = __read_utf16(b, &c, &b->read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0x1F616);

    result = __read_utf16(b, &c, &b->read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0x10FFFF);

    TEST_FINISH(success)
    nix_buffer__free(&p);

    return success;
}

TEST(__read_utf16_data_BE) {
    TEST_START

    struct nix_buffer *p = NULL;
    uint8_t data[] = { 0xFE, 0xFF, 0x01, 0x02 };
    enum nix_err result = nix_buffer__construct_bytes(&p, data, 4);
    ASSERT_EQ(result, NIXERR_NONE);

    struct buffer *b = (struct buffer *)p;
    uint16_t c;

    result = __read_utf16_data(b, &c, &b->read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0x0102);

    TEST_FINISH(success)
    nix_buffer__free(&p);

    return success;
}

TEST(__read_utf16_data_LE) {
    TEST_START

    struct nix_buffer *p = NULL;
    uint8_t data[] = { 0xFF, 0xFE, 0x01, 0x02 };
    enum nix_err result = nix_buffer__construct_bytes(&p, data, 4);
    ASSERT_EQ(result, NIXERR_NONE);

    struct buffer *b = (struct buffer *)p;
    uint16_t c;

    result = __read_utf16_data(b, &c, &b->read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0x0201);

    TEST_FINISH(success)
    nix_buffer__free(&p);

    return success;
}

TEST(__read_byte) {
    TEST_START

    FILE *input = NULL;
    FILE_FROM_STRING(input, (uint8_t *)"\x41\x42\x43", 3);
    struct nix_buffer *p = NULL;
    enum nix_err result = nix_buffer__construct(&p, input, 1);
    ASSERT_EQ(result, NIXERR_NONE);

    struct buffer *b = (struct buffer *)p;
    uint8_t c;

    result = __read_byte(b, &c, &b->read, true);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0x41);

    result = __read_byte(b, &c, &b->read, true);
    ASSERT_EQ(result, NIXERR_BUF_EXHAUST);

    result = __read_byte(b, &c, &b->read, false);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 0x42);

    TEST_FINISH(success)
    CLOSE_FILE(input);
    nix_buffer__free(&p);

    return success;
}

TEST(__check_bounds) {
    TEST_START

    FILE *input = NULL;
    FILE_FROM_STRING(input, (uint8_t *)"\x41\x42\x43", 3);
    struct nix_buffer *p = NULL;
    enum nix_err result = nix_buffer__construct(&p, input, 1);
    ASSERT_EQ(result, NIXERR_NONE);

    struct buffer *b = (struct buffer *)p;

    ASSERT_TRUE(b->buffer_ready[BUFFER_LEFT]);
    ASSERT_FALSE(b->buffer_ready[BUFFER_RIGHT]);
    ASSERT_EQ(*b->left, 0x41);
    ASSERT_EQ(*b->right, 0x00);

    b->buffer_ready[BUFFER_RIGHT] = true;
    result = __check_bounds(b, b->read);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ_M(*b->right, 0x00, "Didn't load data into ready buffer");

    b->buffer_ready[BUFFER_RIGHT] = false;
    result = __check_bounds(b, b->read);
    ASSERT_EQ(result, NIXERR_NONE);

    ASSERT_FALSE(b->buffer_ready[BUFFER_LEFT]);
    ASSERT_TRUE(b->buffer_ready[BUFFER_RIGHT]);
    ASSERT_EQ(*b->left, 0x41);
    ASSERT_EQ(*b->right, 0x42);

    result = __check_bounds(b, b->right);
    ASSERT_EQ(result, NIXERR_BUF_EXHAUST);

    b->read = b->right;
    b->peek = b->right;
    b->lexeme_start = b->right;
    b->lexeme_end = b->right;

    result = __check_bounds(b, b->read);
    ASSERT_EQ(result, NIXERR_NONE);

    ASSERT_TRUE(b->buffer_ready[BUFFER_LEFT]);
    ASSERT_FALSE(b->buffer_ready[BUFFER_RIGHT]);
    ASSERT_EQ(*b->left, 0x43);
    ASSERT_EQ(*b->right, 0x42);

    ASSERT_NULL(b->eof);

    b->read = b->left;
    b->peek = b->left;
    b->lexeme_start = b->left;
    b->lexeme_end = b->left;

    result = __check_bounds(b, b->read);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(b->eof, b->right);

    b->read = b->right;

    result = __check_bounds(b, b->read);
    ASSERT_EQ(result, NIXERR_BUF_EOF);

    result = __check_bounds(b, b->read);
    ASSERT_EQ(result, NIXERR_BUF_PAST_EOF);

    TEST_FINISH(success)
    CLOSE_FILE(input);
    nix_buffer__free(&p);

    return success;
}

TEST(nix_buffer__reset_peek) {
    TEST_START

    struct nix_buffer *p = NULL;
    enum nix_err result = nix_buffer__construct_bytes(&p, (uint8_t *)"ab", 2);
    ASSERT_EQ(result, NIXERR_NONE);

    struct buffer *b = (struct buffer *)p;
    uint32_t c;

    result = nix_buffer__peek(p, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 'a');
    ASSERT_EQ(b->read, b->left);
    ASSERT_EQ(b->peek, b->left + 0x1);
    ASSERT_POS_EQ(p->read, 1, 1, 0);
    ASSERT_POS_EQ(p->peek, 1, 2, 1);

    result = nix_buffer__reset_peek(p);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(b->read, b->left);
    ASSERT_EQ(b->peek, b->read);
    ASSERT_POS_EQ(p->read, 1, 1, 0);
    ASSERT_POS_EQ(p->peek, 1, 1, 0);

    TEST_FINISH(success)
    nix_buffer__free(&p);

    return success;
}

TEST(__buffer_side) {
    TEST_START

    struct buffer *b = malloc(sizeof(struct buffer));
    ASSERT_NNULL(b);
    memset(b, 0, sizeof(struct buffer));

    enum nix_err result = nix_buffer__init_bytes(b, (uint8_t *)"ab", 2);
    ASSERT_EQ(result, NIXERR_NONE);

    enum buffer_side side;

    result = __buffer_side(b, &side, b->left);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(side, BUFFER_LEFT);

    result = __buffer_side(b, &side, b->right);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(side, BUFFER_RIGHT);

    result = __buffer_side(b, &side, b->left - 0x1);
    ASSERT_EQ(result, NIXERR_BUF_INVPTR);

    result = __buffer_side(b, &side, b->right + b->p.buffer_size);
    ASSERT_EQ(result, NIXERR_BUF_INVPTR);

    TEST_FINISH(success)
    nix_buffer__free((struct nix_buffer **)&b);

    return success;
}

TEST(__load_buffer) {
    TEST_START

    FILE *input = NULL;
    FILE_FROM_STRING(input, (uint8_t *)"abcdef", 6);
    struct nix_buffer *p = NULL;
    enum nix_err result = nix_buffer__construct(&p, input, 2);
    ASSERT_EQ(result, NIXERR_NONE);

    struct buffer *b = (struct buffer *)p;

    ASSERT_TRUE(b->buffer_ready[BUFFER_LEFT]);
    ASSERT_FALSE(b->buffer_ready[BUFFER_RIGHT]);
    ASSERT_EQ(*b->read, 'a');
    ASSERT_EQ(*b->right, 0x0);

    result = __load_buffer(b, BUFFER_RIGHT);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_FALSE(b->buffer_ready[BUFFER_LEFT]);
    ASSERT_TRUE(b->buffer_ready[BUFFER_RIGHT]);
    ASSERT_EQ(*b->right, 'c');

    result = __load_buffer(b, BUFFER_LEFT);
    ASSERT_EQ(result, NIXERR_BUF_EXHAUST);

    b->read = b->right;
    b->peek = b->right;
    b->lexeme_start = b->right;
    b->lexeme_end = b->right;

    result = __load_buffer(b, BUFFER_LEFT);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_TRUE(b->buffer_ready[BUFFER_LEFT]);
    ASSERT_FALSE(b->buffer_ready[BUFFER_RIGHT]);
    ASSERT_EQ(*b->left, 'e');
    ASSERT_EQ(*b->right, 'c');

    result = __load_buffer(b, BUFFER_LEFT);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_TRUE(b->buffer_ready[BUFFER_LEFT]);
    ASSERT_FALSE(b->buffer_ready[BUFFER_RIGHT]);
    ASSERT_EQ(*b->left, 'e');
    ASSERT_EQ(b->eof, b->left);

    TEST_FINISH(success)
    CLOSE_FILE(input);
    nix_buffer__free(&p);

    return success;
}

TEST(__buffer_occupied) {
    TEST_START

    struct nix_buffer *p = NULL;
    enum nix_err result = nix_buffer__construct_bytes(&p, (uint8_t *)"abc", 3);
    ASSERT_EQ(result, NIXERR_NONE);

    struct buffer *b = (struct buffer *)p;
    bool occupied;

    __buffer_occupied(b, &occupied, BUFFER_LEFT);
    ASSERT_TRUE(occupied);

    __buffer_occupied(b, &occupied, BUFFER_RIGHT);
    ASSERT_FALSE(occupied);

    b->read = b->right;
    __buffer_occupied(b, &occupied, BUFFER_RIGHT);
    ASSERT_TRUE(occupied);

    b->read = b->left - 0x1;
    b->peek = b->right;
    __buffer_occupied(b, &occupied, BUFFER_RIGHT);
    ASSERT_TRUE(occupied);

    b->peek = b->right + p->buffer_size;
    b->lexeme_start = b->right;
    __buffer_occupied(b, &occupied, BUFFER_RIGHT);
    ASSERT_TRUE(occupied);

    b->lexeme_start = NULL;
    b->lexeme_end = b->right;
    __buffer_occupied(b, &occupied, BUFFER_RIGHT);
    ASSERT_TRUE(occupied);

    b->lexeme_end = b->right + (p->buffer_size * 2);

    __buffer_occupied(b, &occupied, BUFFER_RIGHT);
    ASSERT_FALSE(occupied);

    __buffer_occupied(b, &occupied, BUFFER_LEFT);
    ASSERT_FALSE(occupied);

    TEST_FINISH(success)
    nix_buffer__free(&p);

    return success;
}

TEST(__increment) {
    TEST_START

    struct buffer *b = malloc(sizeof(struct buffer));
    ASSERT_NNULL(b);
    memset(b, 0, sizeof(struct buffer));

    enum nix_err result = nix_buffer__init_bytes(b, (uint8_t *)"abc", 3);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(b->p.buffer_size, 2);

    __increment(b, &b->read);
    ASSERT_EQ(b->read, b->left + 0x1);

    __increment(b, &b->read);
    ASSERT_EQ(b->read, b->right);

    __increment(b, &b->read);
    ASSERT_EQ(b->read, b->right + 0x1);

    __increment(b, &b->read);
    ASSERT_EQ(b->read, b->left);

    TEST_FINISH(success)
    nix_buffer__free((struct nix_buffer **)&b);

    return success;
}

TEST(nix_buffer__read_lexeme) {
    TEST_START

    struct nix_buffer *p = NULL;
    enum nix_err result = nix_buffer__construct_bytes(&p, (uint8_t *)"abc", 3);
    ASSERT_EQ(result, NIXERR_NONE);

    struct buffer *b = (struct buffer *)p;
    uint32_t c;

    result = nix_buffer__read(p, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 'a');

    result = nix_buffer__peek(p, &c);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_EQ(c, 'b');

    struct nix_lexeme *lexeme = NULL;
    result = nix_buffer__read_lexeme(p, &lexeme, 0);
    ASSERT_EQ(result, NIXERR_NONE);
    ASSERT_NNULL(lexeme);

    ASSERT_EQ(b->lexeme_start, b->read);
    ASSERT_POS_EQ(p->lexeme_start, 1, 3, 2);
    ASSERT_EQ(b->lexeme_end, NULL);
    ASSERT_POS_EQ(p->lexeme_end, 1, 3, 2);
    ASSERT_EQ(b->read, b->left + 0x1);
    ASSERT_POS_EQ(p->read, 1, 3, 2);
    ASSERT_EQ(b->peek, b->read);
    ASSERT_POS_EQ(p->peek, 1, 3, 2);

    TEST_FINISH(success)

    return success;
}
