#ifndef INCLUDE_test_test_h__
#define INCLUDE_test_test_h__

#include <stdbool.h>

#define FAIL_COLOR "\x1b[31m"
#define PASS_COLOR "\x1b[32m"
#define R_COLOR "\x1b[0m"

#define TESTS_START \
    bool __fail_test = false; \
    char *__fail_msg = NULL;

#define PASS_FMT " ... [ " PASS_COLOR "PASS" R_COLOR " ] %s\n"
#define FAIL_FMT " ... [ " FAIL_COLOR "FAIL" R_COLOR " ] %s: %s\n"

#define RUN_TEST(name, description) \
    if (__test_ ## name(&__fail_msg) == true) { \
        printf(PASS_FMT, description); \
        __fail_msg = ""; \
    } else { \
        printf(FAIL_FMT, description, __fail_msg); \
        __fail_test = true; \
        __fail_msg = ""; \
    }

#define TESTS_FINISH return __fail_test ? 1 : 0;

#define TEST(name) bool __test_ ## name(char **fail_msg)
#define DECL_TEST(name) bool __test_ ## name(char **);

#define TEST_START bool __test_succeeded = true;

#define ASSERT_M(expr, msg) \
    if (!(expr)) { \
        *fail_msg = msg; \
        __test_succeeded = false; \
        goto __test_finished; \
    }

#define ASSERT(expr) ASSERT_M(expr, "Assertion failed: " #expr)

#define ASSERT_EQ_M(left, right, msg) ASSERT_M(left == right, msg)
#define ASSERT_EQ(left, right) \
    ASSERT_EQ_M(left, right, \
        "Expected `" #left "` and `" #right "` to be equal")

#define ASSERT_NEQ_M(left, right, msg) ASSERT_M(left != right, msg)
#define ASSERT_NEQ(left, right) \
    ASSERT_NEQ_M(left, right, \
        "Expected `" #left "` and `" #right "` to not be equal")

#define ASSERT_NULL_M(expr, msg) ASSERT_M((expr) == NULL, msg)
#define ASSERT_NULL(expr) \
    ASSERT_NULL_M(expr, "Expected `" #expr "` to be null")

#define ASSERT_NNULL_M(expr, msg) ASSERT_M((expr) != NULL, msg)
#define ASSERT_NNULL(expr) \
    ASSERT_NNULL_M(expr, "Expected `" #expr "` to not be null")

#define ASSERT_TRUE_M(expr, msg) ASSERT_M((expr) == true, msg)
#define ASSERT_TRUE(expr) \
    ASSERT_TRUE_M(expr, "Expected `" #expr "` to be true")

#define ASSERT_FALSE_M(expr, msg) ASSERT_M((expr) == false, msg)
#define ASSERT_FALSE(expr) \
    ASSERT_FALSE_M(expr, "Expected `" #expr "` to be false")

#define ASSERT_POS_EQ_M(expr, exp_row, exp_col, exp_abs, msg) \
    ASSERT_EQ_M(expr->row, exp_row, msg) \
    ASSERT_EQ_M(expr->col, exp_col, msg) \
    ASSERT_EQ_M(expr->abs, exp_abs, msg)
#define ASSERT_POS_EQ(expr, row, col, abs) \
    ASSERT_POS_EQ_M(expr, row, col, abs, "Expected `" #expr \
        "` to equal position (" #row ", " #col " / " #abs ")")

#define TEST_FINISH(success) \
    __test_finished:; \
    bool success = __test_succeeded;

#define CLOSE_FILE(file) \
    if (file != NULL) { \
        fclose(file); \
        file = NULL; \
    }

#define FILE_FROM_STRING(file, data, length) \
    CLOSE_FILE(file); \
    file = file_from_string(data, length); \
    ASSERT_NNULL_M(file, "Failed to open temp file");

FILE* file_from_string(uint8_t *, size_t);
void random_string(char *, size_t);

#endif
