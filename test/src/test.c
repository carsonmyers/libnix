#include <stdio.h>
#include <stdlib.h>

#include "test.h"

static const size_t filename_length = 16;
FILE *file_from_string(uint8_t *data, size_t length) {
    char *filename = malloc(sizeof(char) * filename_length + 1);
    if (filename == NULL) {
        return NULL;
    }

    random_string(filename, filename_length);

    FILE *writer = fopen(filename, "w+");
    if (writer == NULL) {
        free(filename);
        return NULL;
    }

    size_t written = fwrite(data, sizeof(uint8_t), length, writer);
    fclose(writer);

    if (written != length) {
        free(filename);
        return NULL;
    }

    FILE *reader = fopen(filename, "r");
    free(filename);
    return reader;
}

static const char charset[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
void random_string(char *out, size_t length) {
    for (size_t i = 0; i < length; i++) {
        size_t key = rand() % (sizeof(charset) - 1);
        out[i] = charset[key];
    }
}
