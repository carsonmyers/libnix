#ifndef INCLUDE_libnix_reader_h__
#define INCLUDE_libnix_reader_h__

#include <stdio.h>
#include <stdint.h>

#include "libnix/common.h"
#include "libnix/decode.h"

NIX_BEGIN_DECL

NIX_EXTERN(enum nix_err)
nix_reader__construct_file(
    void **out,
    FILE *in,
    enum nix_encoding encoding
);

NIX_EXTERN(enum nix_err)
nix_reader__construct_cstring(
    void **out,
    char *in,
    enum nix_encoding encoding
);

NIX_EXTERN(enum nix_err)
nix_reader__construct_mem(
    void **out,
    uint8_t *in,
    int length,
    enum nix_encoding encoding
);

NIX_EXTERN(enum nix_err)
nix_reader__read(void *reader, uint8_t *out, size_t *num_read, size_t count);

NIX_EXTERN(void)
nix_reader__free(void **out);

NIX_END_DECL

#endif

