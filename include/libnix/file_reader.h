#ifndef INCLUDE_libnix_file_reader_h__
#define INCLUDE_libnix_file_reader_h__

#include <stdio.h>
#include "libnix/common.h"

NIX_BEGIN_DECL

NIX_EXTERN(enum nix_err)
nix_file_reader__construct(void **out, FILE *in);

NIX_EXTERN(enum nix_err)
nix_file_reader__load(void *reader, uint8_t *out, uint32_t count);

NIX_EXTERN(void)
nix_file_reader__free(void **out);

NIX_END_DECL

#endif
