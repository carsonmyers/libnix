#ifndef INCLUDE_libnix_error_h__
#define INCLUDE_libnix_error_h__

#include "libnix/common.h"

NIX_BEGIN_DECL

enum nix_err {
    NIXERR_NONE = 0,
    NIXERR_NULL,
    NIXERR_INVPTR,
    NIXERR_NOMEMORY,

    NIXERR_READ = 0x0100,
    NIXERR_READ_INPUT,
    NIXERR_READ_EXHAUST,
    NIXERR_READ_INVPTR,
    NIXERR_READ_MAX_BUFFER_SIZE,

    NIXERR_DECODE = 0x0200,
    NIXERR_DECODE_UNSUPPORTED,
    NIXERR_DECODE_UNKNOWN,
    NIXERR_DECODE_INVCODEPOINT,

    NIXERR_BUF = 0x0300,
    NIXERR_BUF_INVLEN,
    NIXERR_BUF_INVPTR,
    NIXERR_BUF_EXHAUST,
    NIXERR_BUF_INVCHAR,
    NIXERR_BUF_FILE,
    NIXERR_BUF_EOF,
    NIXERR_BUF_PAST_EOF
};

NIX_END_DECL

#endif
