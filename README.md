## libnix parser generator library

This project was started in C, but is now being built in Rust. As such, this
code is incomplete and will no longer be updated.

Presently, this project is able to load files into a ring buffer. The buffer
is designed to stream through very large files without holding the file in
memory, while matching literal characters and codepoints and maintaining a
"lexeme" which can be consumed once the match of a token is complete. This
includes the text making up the token, and the file line and column position
where it was found.

Upon loading a file into the buffer, it is translated from a variety of text
encodings to utf-8. Each character matched from the buffer is a unicode
codepoint, such that matching any particular grapheme may consume multiple
bytes from the buffer.

There is also a python program which downloads and parses an arbitrary version
of the unicode character database and rewrites it into C structs so that the
entire UCD is loaded into the program at compile-time. This increases the size
of the binary but saves on runtime.

The basic multilingual plane (`U+0000..U+10000`) is stored in a contiguous array
to make lookups of the most common characters very fast. The rest is stored in a
sparse array where runs of adjacent characters are packed into a contiguous
block, and a middle lookup step to translate a codepoint into a memory location
is needed. An array of run structs can be used to binary search for the
requested codepoint, with a match consisting of finding a run where the
codepoint is <= the run-end and >= the run-start. The memory location is then
the run start pointer plus the difference between the run-start and the lookup
codepoint.

The main source is in the `src` directory; a few tests are located in the
`test/src` directory, which are useful to see how the buffer is meant to be
used.

All code in this repository (except data in the `ucd/database` directory) is
wholly owned and copyrighted by Carson Myers