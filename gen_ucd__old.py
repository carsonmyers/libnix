#!/usr/bin/env python3
from fractions import Fraction
import argparse
import re
import urllib.request
import zipfile
import os


class CharacterDecomposition:
    def __init__(self, raw):
        m = re.match('(?:<(?P<type>\w+)>\s+)?(?P<code_points>(?:[0-9A-F]+\s*)+)', raw)

        if m is None:
            self.type = None
            self.code_points = []
        else:
            self.type = m.group("type")
            if self.type is None:
                self.type = "canonical"

            self.code_points = list(map(lambda c: int(c, 16), m.group("code_points").split()))


class UnihanVariant:
    linePattern = re.compile(r"""
        ^
        U\+(?P<code_point>[0-9A-F]+)
        \s+
        (?P<type>[a-zA-Z]+)
        \s+
        (?P<variants>[U+0-9a-zA-Z<,:\s]+)
        \s+
        $
    """, re.VERBOSE)

    variantsPattern = re.compile(r"""
        U\+([0-9A-F]+)
        (?:
            <
            (?:k[a-zA-Z0-9]+(?::[TBZFJ]+)?)
            (?:,k[a-zA-Z0-9]+(?::[TBZFJ]+)?)*
        )?
        \s*
    """, re.VERBOSE)

    def __init__(self, raw):
        m = UnihanVariant.linePattern.match(raw)
        if m is None:
            print("linedPattern failed to match '{0}'".format(raw))

        self.code_point = int(m.group("code_point"), 16)
        self.type = m.group("type")

        vm = UnihanVariant.variantsPattern.findall(m.group("variants"))
        self.variants = [int(c, 16) for c in vm]


class GeneralCategory:
    _categoryMap = {
        "Lu": {"major": "Letter", "minor": "uppercase", "type": "Graphic"},
        "Ll": {"major": "Letter", "minor": "lowercase", "type": "Graphic"},
        "Lt": {"major": "Letter", "minor": "titlecase", "type": "Graphic"},
        "Lm": {"major": "Letter", "minor": "modifier", "type": "Graphic"},
        "Lo": {"major": "Letter", "minor": "other", "type": "Graphic"},
        "Mn": {"major": "Mark", "minor": "nonspacing", "type": "Graphic"},
        "Mc": {"major": "Mark", "minor": "spacing combining", "type": "Graphic"},
        "Me": {"major": "Mark", "minor": "enclosing", "type": "Graphic"},
        "Nd": {"major": "Number", "minor": "decimal digit", "type": "Graphic"},
        "Nl": {"major": "Number", "minor": "letter", "type": "Graphic"},
        "Pc": {"major": "Punctuation", "minor": "connector", "type": "Graphic"},
        "Pd": {"major": "Punctuation", "minor": "dash", "type": "Graphic"},
        "Ps": {"major": "Punctuation", "minor": "open", "type": "Graphic"},
        "Pe": {"major": "Punctuation", "minor": "close", "type": "Graphic"},
        "Pi": {"major": "Punctuation", "minor": "initial quote", "type": "Graphic"},
        "Pf": {"major": "Punctuation", "minor": "final quote", "type": "Graphic"},
        "Po": {"major": "Punctuation", "minor": "other", "type": "Graphic"},
        "Sm": {"major": "Symbol", "minor": "math", "type": "Graphic"},
        "Sc": {"major": "Symbol", "minor": "currency", "type": "Graphic"},
        "Sk": {"major": "Symbol", "minor": "modifier", "type": "Graphic"},
        "So": {"major": "Symbol", "minor": "other", "type": "Graphic"},
        "Zs": {"major": "Separator", "minor": "space", "type": "Graphic"},
        "Zl": {"major": "Separator", "minor": "line", "type": "Format"},
        "Zp": {"major": "Separator", "minor": "paragraph", "type": "Format"},
        "Cc": {"major": "Other", "minor": "control", "type": "Control"},
        "Cf": {"major": "Other", "minor": "format", "type": "Format"},
        "Cs": {"major": "Other", "minor": "surrogate", "type": "Surrogate"},
        "Co": {"major": "Other", "minor": "private use", "type": "Private-use"},
        "Cn": {"major": "Other", "minor": "not assigned", "type": "Noncharacter"},
    }

    def __init__(self, raw):
        self.categoryId = raw
        if raw in self._categoryMap.keys():
            self.category = self._categoryMap[raw]
        else:
            self.category = self._categoryMap["Cn"]

    def __str__(self):
       return "{maj}/{min}".format(maj=self.category["major"], min=self.category["minor"])


class CodePoint:
    def __init__(self, ucd_data):
        values = ucd_data.strip().split(";")

        self.code = int(values[0], 16)
        self.name = values[1]

        self.category = GeneralCategory(values[2])
        self.combining_class = int(values[3], 10)
        self.bidirectional_category = values[4]
        self.decomposition_mapping = CharacterDecomposition(values[5])

        self.decimal_digit_value = int(values[6], 10) if values[6] else None
        self.digit_value = int(values[7], 10) if values[7] else None
        self.numeric_value = Fraction(values[8]) if values[8] else None

        self.mirrored = True if values[9] == 'Y' else False
        self.old_name = values[10]
        self.ISO_10646_comment = values[11]

        self.uppercase_mapping = int(values[12], 16) if values[12] else 0
        self.lowercase_mapping = int(values[13], 16) if values[13] else 0
        self.titlecase_mapping = int(values[14], 16) if values[14] else 0

    def __str__(self):
        name = self.name
        if len(name) > 35:
            name = "{0}...".format(name[:32])


        return "{code:<6} \"{name:<35}\" ({cat:<26}) {uc}/{lc}/{tc}".format(**{
            "code": self.code,
            "name": name,
            "cat": self.category,
            "ccl": self.combining_class,
            "dec": "{0} <{1}>".format(
                self.decomposition_mapping.type,
                self.decomposition_mapping.code_points),
            "uc": self.uppercase_mapping,
            "lc": self.lowercase_mapping,
            "tc": self.titlecase_mapping,
            "ddv": self.decimal_digit_value,
            "dv": self.digit_value,
            "nv": self.numeric_value,
        })


class UCD:
    def __init__(self, args):
        ucd_file = open("{0}/UnicodeData.txt".format(args.ucd_destination))
        var_file = open("{0}/Unihan_Variants.txt".format(args.ucd_destination))

        self.variants = []
        last_code_point = None
        current_code_point = {}

        for entry in var_file:
            if entry[0] in ["#", "\n"] or len(entry) == 0:
                continue

            variant = UnihanVariant(entry)
            if variant.type not in args.variants:
                continue

            if last_code_point != variant.code_point:
                if last_code_point is not None:
                    self.variants.append(current_code_point)

                last_code_point = variant.code_point
                current_code_point = {
                    "code_point": variant.code_point,
                    "variants": {}
                }

            if (variant.code_point == last_code_point):
                current_code_point["variants"][variant.type] = variant

        self.bmp = [None] * 0x10000
        self.sparse = []
        self.sparse_runs = []
        run_start = None
        last_code_point = None

        for entry in ucd_file:
            point = CodePoint(entry)
            if point.name[0] == "<":
                print("0x{0:X} {1}".format(point.code, point.name))

            if point.code < 0x10000:
                self.bmp[point.code] = point
            else:
                self.sparse.append(point)

                if run_start is None:
                    run_start = point.code
                    last_code_point = point.code
                if point.code - last_code_point > 1:
                    self.sparse_runs.append((run_start, last_code_point))
                    run_start = point.code

                last_code_point = point.code


def code_point_struct(point, decompositions, decomposition_map):
    if point is None:
        return "{0}"
    else:
        dec_type = point.decomposition_mapping.type
        if not dec_type or dec_type not in decompositions:
            dec_ptr = "NULL"
        else:
            dec_ptr = "&ucd_decompositions[{0}]".format(len(decomposition_map))
            decomposition_map.append(point.decomposition_mapping)

        components = [
            ("code_point", "(uint32_t){0:#06X}u".format(point.code)),
            ("general_category", point.category.categoryId),
            ("combining_class", "(uint8_t){0}u".format(point.combining_class)),
            ("decomposition", dec_ptr),
            ("uppercase_mapping", "(uint32_t){0:#06X}u".format(point.uppercase_mapping)),
            ("lowercase_mapping", "(uint32_t){0:#06X}u".format(point.lowercase_mapping)),
            ("titlecase_mapping", "(uint32_t){0:#06X}u".format(point.titlecase_mapping))
        ]

        struct_fields = map(lambda f: ".{0} = {1}".format(*f), components)
        return "{{ {0} }}".format(", ".join(struct_fields))


def decomposition_struct(decomposition, decomposition_max_length):
    code_points = list(map(lambda c: "{0:#06x}".format(c), decomposition.code_points))
    if len(code_points) > decomposition_max_length:
        code_points = code_points[:decomposition_max_length]

    code_points_s = "{{ {0} }}".format(", ".join(code_points))

    components = [
        ("tag", "CDMT_{0}".format(decomposition.type.upper())),
        ("length", "(size_t){0}".format(len(code_points))),
        ("data", code_points_s)
    ]

    struct_fields = map(lambda f: ".{0} = {1}".format(*f), components)
    return "{{ {0} }}".format(", ".join(struct_fields))


def run_struct(run, offset):
    components = [
        ("first", "(uint32_t){0:#06X}u".format(run[0])),
        ("last", "(uint32_t){0:#06X}u".format(run[1])),
        ("offset", "(size_t){0}".format(offset))
    ]

    struct_fields = map(lambda f: ".{0} = {1}".format(*f), components)
    return "{{ {0} }}".format(", ".join(struct_fields))


def write_ucd_c(ucd, args):
    dec_map = []

    with open("{0}/ucd.c".format(args.c_destination), "w+") as f:
        f.write("#include <stdint.h>\n")
        f.write("#include <stdlib.h>\n")
        f.write("#include \"ucd.h\"\n\n")

        f.write("struct ucd_data ucd_bmp[0x10000] = {\n")
        for code in range(0x0000, 0x10000):
            point = ucd.bmp[code]

            if point is None:
                name = "unassigned"
            elif len(point.name) > 30:
                name = point.name[:30 - 3] + "..."
            else:
                name = "{0:<30}".format(point.name)

            name_comment = "/* {0:#06X} - {1} */".format(code, name)
            struct_cast = "(struct code_point)"
            struct = code_point_struct(point, args.decompositions, dec_map)
            f.write("    {0} {1} {2},\n"
                    .format(name_comment, struct_cast, struct))
        f.write("};\n\n")

        f.write("struct ucd_data ucd_sparse_code_points[{0}] = {{\n"
                .format(len(ucd.sparse)))
        for point in ucd.sparse:
            if len(point.name) > 30:
                name = point.name[:30 - 3] + "..."
            else:
                name = "{0:<30}".format(point.name)

            name_comment = "/* {0:#06X} - {1} */".format(point.code, name)
            struct_cast = "(struct code_point)"
            struct = code_point_struct(point, args.decompositions, dec_map)
            f.write("    {0} {1} {2},\n"
                    .format(name_comment, struct_cast, struct))
        f.write("};\n\n")

        sparse_offset = 0
        f.write("struct ucd_sparse_run ucd_sparse_runs[{0}] = {{\n"
                .format(len(ucd.sparse_runs)))
        for run in ucd.sparse_runs:
            struct_cast = "(struct code_point_run)"
            struct = run_struct(run, sparse_offset)
            f.write("    {0} {1},\n".format(struct_cast, struct))
            sparse_offset += run[1] - run[0] + 1
        f.write("};\n\n")

        f.write("struct ucd_decomposition ucd_decompositions[{0}] = {{\n"
                .format(len(dec_map)))
        for dec in dec_map:
            struct_cast = "(struct ucd_decomposition)"
            struct = decomposition_struct(dec, args.decomposition_max_length)
            f.write("    {0} {1},\n".format(struct_cast, struct))
        f.write("};\n")

    return len(dec_map)


def write_ucd_h(ucd, dec_map_size, args):
    header = """#include <stdint.h>

enum ucd_category {
    GC_L = 0x00,
    Lu, Ll, Lt, Lm, Lo,

    GC_M = 0x10,
    Mn, Mc, Me,

    GC_N = 0x20,
    Nd, Nl, No,

    GC_P = 0x40,
    Pc, Pd, Ps, Pe, Pi, Pf, Po,

    GC_S = 0x80,
    Sm, Sc, Sk, So,

    GC_Z = 0x100,
    Zs, Zl, Zp,

    GC_C = 0x200,
    Cc, Cf, Cs, Co, Cn
};

enum ucd_decomposition_tag {
    CDMT_CANONICAL,
    CDMT_FONT,
    CDMT_NOBREAK,
    CDMT_INITIAL,
    CDMT_MEDIAL,
    CDMT_FINAL,
    CDMT_ISOLATED,
    CDMT_CIRCLE,
    CDMT_SUPER,
    CDMT_SUB,
    CDMT_VERTICAL,
    CDMT_WIDE,
    CDMT_NARROW,
    CDMT_SMALL,
    CDMT_SQUARE,
    CDMT_FRACTION,
    CDMT_COMPAT
};

struct ucd_decomposition {
    enum ucd_decomposition_tag tag;
    size_t decomposition_length;
    uint32_t decomposition[4];
};

enum ucd_han_variant_type {
    CDHV_COMPATIBILITY,
    CDHV_SEMANTIC,
    CDHV_SPECIALIZED_SEMANTIC,
    CDHV_SIMPLIFIED,
    CDHV_TRADITIONAL
};

struct ucd_han_variant {
    enum ucd_han_variant_type type;
    uint32_t code_point;
}

struct ucd_han_variants {
    struct ucd_han_variant variant[4];
}

struct ucd_data {
    uint32_t code_point;
    enum ucd_category general_category;
    uint8_t combining_class;
    struct ucd_decomposition *decomposition;
    uint32_t uppercase_mapping;
    uint32_t lowercase_mapping;
    uint32_t titlecase_mapping;
    struct ucd_han_variants *variants;
};

struct ucd_sparse_run {
    uint32_t first;
    uint32_t last;
    size_t offset;
};

"""
    with open("{0}/ucd.h".format(args.c_destination), "w+") as f:
        f.write(header)
        f.write("#define UCD_BMP_LENGTH 0x10000\n")
        f.write("#define UCD_SPARSE_CODE_POINTS_LENGTH {0}\n"
                .format(len(ucd.sparse)))
        f.write("#define UCD_SPARSE_RUNS_LENGTH {0}\n"
                .format(len(ucd.sparse_runs)))
        f.write("#define UCD_DECOMPOSITIONS_LENGTH {0}\n"
                .format(dec_map_size))
        f.write("\n")

        f.write("struct ucd_data ucd_bmp[UCD_BMP_LENGTH];\n")
        f.write("struct ucd_data ucd_sparse_code_points[{0}];\n"
                .format("UCD_SPARSE_CODE_POINTS_LENGTH"));
        f.write("struct ucd_sparse_run ucd_sparse_runs[{0}];\n"
                .format("UCD_SPARSE_RUNS_LENGTH"))
        f.write("struct ucd_decomposition ucd_decompositions[{0}];\n"
                .format("UCD_DECOMPOSITIONS_LENGTH"))


def download_ucd(source, dest):
    ucd_source = "{0}/ucd/UCD.zip".format(source)
    ucd_dest = "{0}/UCD.zip".format(dest)

    unihan_source = "{0}/ucd/Unihan.zip".format(source)
    unihan_dest = "{0}/Unihan.zip".format(dest)

    print("Downloading {0}...".format(ucd_source))
    urllib.request.urlretrieve(ucd_source, ucd_dest)
    print("Downloading {0}...".format(unihan_source))
    urllib.request.urlretrieve(unihan_source, unihan_dest)

    with zipfile.ZipFile(ucd_dest) as zip:
        print("Extracting UnicodeData.txt...")
        zip.extract("UnicodeData.txt", dest)

    with zipfile.ZipFile(unihan_dest) as zip:
        print("Extracting Unihan_Variants.txt...")
        zip.extract("Unihan_Variants.txt", dest)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--update-ucd",
                        help="Download the latest version of the "
                        "Unicode Character Database",
                        action="store_true")
    parser.add_argument("-s", "--ucd-source",
                        help="Unicode character database source URL. "
                        "Only useful with --update-ucd",
                        default="http://www.unicode.org/Public/UCD/latest")
    parser.add_argument("-d", "--ucd-destination",
                        help="Destination for UCD files. "
                        "Only useful with --update-ucd",
                        default="./ucd")
    parser.add_argument("-D", "--c-destination",
                        help="Destination for ucd.c file",
                        default="./src")
    parser.add_argument("--decompositions",
                        help="Decomposition classes to include in output",
                        default="canonical", type=str, nargs="+", metavar="D")
    parser.add_argument("--variants",
                        help="CJK Variant types to include in output",
                        default="kZVariant", type=str, nargs="+", metavar="V")
    parser.add_argument("--decomposition-max-length",
                        help="Max length for decomposition. "
                        "Longer ones will be truncated",
                        default=4, type=int)
    args = parser.parse_args()

    if not os.path.exists(args.ucd_destination):
        os.makedirs(args.ucd_destination)

    if args.update_ucd:
        download_ucd(args.ucd_source, args.ucd_destination)

    print("Loading Unicode Character Database...")
    ucd = UCD(args)

    print("Loaded {0} variant(s)".format(len(ucd.variants)))

    for v in list(filter(lambda v: len(v["variants"]) > 0, ucd.variants))[:25]:
        print("{0:X}".format(v["code_point"]), *[
            "{0}/{1}".format(
                i.type,
                ",".join(["{0:X}".format(v) for v in i.variants])
            )
            for _, i in v["variants"].items()
        ])
    print("Loaded unicode character database")
    print("Basic Multilingual Plane loaded into contiguous array (65536 code points)")
    print("{0} code points loaded into sparse array".format(len(ucd.sparse)))

    if not os.path.exists(args.c_destination):
        os.makedirs(args.c_destination)

    dec_map_size = write_ucd_c(ucd, args)
    write_ucd_h(ucd, dec_map_size, args)


if __name__ == "__main__":
    main()
