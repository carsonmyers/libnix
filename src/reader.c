#include "reader.h"

enum nix_err
nix_reader__construct_file(
    void **out,
    FILE *in,
    enum nix_encoding encoding
) {
    struct file_reader *r = NULL;
    ALLOC(r, sizeof(struct file_reader));

    TRY(nix_reader__init_file(r, in, encoding));
    *out = (void *)r;

    EXCEPT(err)
    CATCH(NIXERR_NOMEMORY) {
        nix_reader__free((void **)&r);
    }

    return err;
}

enum nix_err
nix_reader__init_file(
    struct file_reader *out,
    FILE *in,
    enum nix_encoding encoding
) {
    TRY(__init_reader((struct reader *)out, encoding));
    out->in = in;

    EXCEPT(err)
    return err;
}

enum nix_err
nix_reader__construct_cstring(
    void **out,
    char *in,
    enum nix_encoding encoding
) {
    struct cstring_reader *r = NULL;
    ALLOC(r, sizeof(struct cstring_reader));

    TRY(nix_reader__init_cstring(r, in, encoding));
    *out = (void *)r;

    EXCEPT(err)
    CATCH(NIXERR_NOMEMORY) {
        nix_reader__free((void **)&r);
    }

    return err;
}

enum nix_err
nix_reader__init_cstring(
    struct cstring_reader *out,
    char *in,
    enum nix_encoding encoding
) {
    TRY(__init_reader((struct reader *)out, encoding));
    out->in = in;

    EXCEPT(err)
    return err;
}

enum nix_err
nix_reader__construct_mem(
    void **out,
    uint8_t *in,
    int length,
    enum nix_encoding encoding
) {
    struct mem_reader *r = NULL;
    ALLOC(r, sizeof(struct mem_reader));

    TRY(nix_reader__init_mem(r, in, length, encoding));
    *out = (void *)r;

    EXCEPT(err)
    CATCH(NIXERR_NOMEMORY) {
        nix_reader__free((void **)&r);
    }

    return err;
}

enum nix_err
nix_reader__init_mem(
    struct mem_reader *out,
    uint8_t *in,
    int length,
    enum nix_encoding encoding
) {
    TRY(__init_reader((struct reader *)out, encoding));
    out->in = in;

    EXCEPT(err)
    return err;
}

enum nix_err
__init_reader(struct reader *out, enum nix_encoding encoding) {
    ALLOC(out->buffer, READER_BUFFER_INIT_SIZE);
    out->buffer_size = READER_BUFFER_INIT_SIZE;
    out->read = NULL;
    out->input_exhausted = false;
    out->data_length = 0;

    out->decode_state = (struct nix_decode_state) { 0 };

    TRY(__load_reader(out));
    if (encoding == NIXENC_DETECT) {
        size_t bytes_read = 0;
        TRY(__detect_encoding(out, &bytes_read, &encoding));
        out->read = out->buffer + bytes_read;
    }

    switch (encoding) {
    case NIXENC_UTF8:
        reader->decoder = __decode_utf8;
        break;
    case NIXENC_UTF16_LE:
        reader->decoder = __decode_utf16_le;
        break;
    case NIXENC_UTF16_BE:
        reader->decoder = __decode_utf16_be;
        break;
    case NIXENC_UTF32_LE:
        reader->decoder = __decode_utf32_le;
        break;
    case NIXENC_UTF32_BE:
        reader->decoder = __decode_utf32_be;
        break;
    case NIXENC_8859_1:
        reader->decoder = __decode_8859_1;
        break;
    default:
        return NIXERR_DECODE_UNSUPPORTED;
    }

    EXCEPT(err);
    return err;
}

enum nix_err
nix_reader__read(void *reader, uint8_t *out, size_t *num_read, size_t count) {
    struct reader *r = (struct reader *)reader;

    bool requires_load = r->read == NULL || r->read - r->buffer == 0;
    if (requires_load && r->input_exhausted) {
        return NIXERR_READ_EXHAUST;
    }

    switch (r->reader_type) {
    case NIX_FILE_READER:
        if (requires_load) {
            TRY(__load_file((struct file_reader *)r));
        }

        TRY(__read_file((struct file_reader *)r, out, num_read, count));
        break;

    case NIX_CSTRING_READER:
        if (requires_load) {
            TRY(__load_cstring((struct string_reader *)r));
        }

        TRY(__read_cstring((struct cstring_reader *)r, out, num_read, count));
        break;

    case NIX_MEM_READER:
        if (requires_load) {
            TRY(__load_mem((struct mem_reader *)r));
        }

        TRY(__read_mem((struct mem_reader *)r, out, num_read, count));
        break;

    default:
        return NIXERR_INVPTR;
    }

    EXCEPT(err)
    return err;
}

enum nix_err
__read_file(
    struct file_reader *read,
    uint8_t *out,
    size_t *num_read,
    size_t count
) {

}

enum nix_err
nix_reader__load(struct reader *reader) {
    switch (reader->reader_type) {
    case NIX_FILE_READER:
        TRY(__load_file((struct file_reader *)reader));
        break;

    case NIX_CSTRING_READER:
        TRY(__load_cstring((struct cstring_reader *)reader));
        break;

    case NIX_MEM_READER:
        TRY(__load_mem((struct mem_reader *)reader));
        break;

    default:
        return NIXERR_INVPTR;
    }

    EXCEPT(err);
    return err;
}

enum nix_err
__load_file(struct file_reader *reader) {
    if (reader->p.input_exhausted) {
        return NIXERR_INPUT;
    }

    uint8_t *read = reader->p.read;
    uint8_t *buf = reader->p.buffer;

    if (read == buf) {
        return NIXERR_READ_EXHAUST;
    }

    // Check if the read pointer is somewhere in the middle of the buffer,
    // so any un-read data can be shuffled to the front
    if (read > buf && read - buf < reader->p.buffer_size) {
        uint8_t *write = buf;
        uint8_t *end = buf + reader->p.buffer_size;

        while (read < end) {
            *write = *read;

            write += 1;
            read += 1;
        }

        read = write;
    } else {
        // If the read pointer was at the end (or beyond, or NULL, or
        // whatever) then just set it to the beginning of the buffer
        read = buf;
    }

    size_t i_to_read = buffer_size - (read - buf);
    size_t i_size = sizeof(uint8_t);
    size_t bytes_read = fread(read, i_size, bytes_to_read, reader->in);

    if (bytes_read != i_to_read * i_size) {
        if (ferror(reader->in) || !feof(reader->in)) {
            return NIXERR_READ_INPUT;
        } else {
            reader->p.input_exhausted = true;
        }
    }

    reader->p.data_length = bytes_read + (read - buf);
}

enum nix_err
__read_cstring(
    struct cstring_reader *read,
    uint8_t out,
    size_t *num_read,
    size_t count
) {
}

enum nix_err __load_cstring(struct cstring_reader *read) {
}

enum nix_err
__read_mem(
    struct mem_reader *read,
    uint8_t *out,
    size_t *num_read,
    size_t count
) {
}

enum nix_err
__load_mem(struct mem_reader *read) {
}

void
nix_reader__free(void **out) {
    if (*out == NULL) return;

    struct reader *r = (struct reader *)*out;
    FREE(r->buffer);

    FREE(r);
    *out = NULL;
}
