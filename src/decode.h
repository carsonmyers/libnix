#ifndef INCLUDE_decode_h__
#define INCLUDE_decode_h__

#include <stdio.h>
#include <stdint.h>

#include "libnix/decode.h"
#include "libnix/error.h"
#include "ucd.h"

struct nix_decode_state {
    uint8_t *src,

    size_t size_dest;
    size_t bytes_written;

    // For when a decoding is more bytes than what are left in the destination
    // buffer - save the last partially decoded code point and the number of
    // bytes already decoded
    uint32_t code_point;
    size_t decoded_bytes;
    size_t skip_bytes;
}

enum nix_err
nix_decode__detect_encoding(
    struct reader *reader,
    size_t *bytes_read,
    enum nix_encoding *encoding
);

enum nix_err
nix_decode__utf8(uint8_t *dest, struct nix_decode_state *state);

enum nix_err
nix_decode__utf16_le(uint8_t *dest, struct nix_decode_state *state);

enum nix_err
nix_decode__utf16_be(uint8_t *dest, struct nix_decode_state *state);

enum nix_err
nix_decode__utf32_le(uint8_t *dest, struct nix_decode_state *state);

enum nix_err
nix_decode__utf32_be(uint8_t *dest, struct nix_decode_state *state);

enum nix_err
nix_decode__8859_1(uint8_t *dest, struct nix_decode_state *state);

#endif
