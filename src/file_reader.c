#include <stdio.h>

#include "decode.h"
#include "error.h"
#include "file_reader.h"

enum nix_err
nix_file_reader__construct(void **out, FILE *in) {
}

enum nix_err
nix_file_reader__init(struct file_reader *out, FILE *in) {
}

enum nix_err
nix_file_reader__load(void *reader, uint8_t *out, int count) {
}

void nix_file_reader__free(void **out) {
}
