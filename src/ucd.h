#include <stdint.h>

enum ucd_category {
    GC_L = 0x00,
    Lu, Ll, Lt, Lm, Lo,

    GC_M = 0x10,
    Mn, Mc, Me,

    GC_N = 0x20,
    Nd, Nl, No,

    GC_P = 0x40,
    Pc, Pd, Ps, Pe, Pi, Pf, Po,

    GC_S = 0x80,
    Sm, Sc, Sk, So,

    GC_Z = 0x100,
    Zs, Zl, Zp,

    GC_C = 0x200,
    Cc, Cf, Cs, Co, Cn
};

enum ucd_decomposition_tag {
    CDMT_CANONICAL,
    CDMT_FONT,
    CDMT_NOBREAK,
    CDMT_INITIAL,
    CDMT_MEDIAL,
    CDMT_FINAL,
    CDMT_ISOLATED,
    CDMT_CIRCLE,
    CDMT_SUPER,
    CDMT_SUB,
    CDMT_VERTICAL,
    CDMT_WIDE,
    CDMT_NARROW,
    CDMT_SMALL,
    CDMT_SQUARE,
    CDMT_FRACTION,
    CDMT_COMPAT
};

struct ucd_decomposition {
    enum ucd_decomposition_tag tag;
    size_t decomposition_length;
    uint32_t decomposition[4];
};

enum ucd_han_variant_type {
    CDHV_COMPATIBILITY,
    CDHV_SEMANTIC,
    CDHV_SPECIALIZED_SEMANTIC,
    CDHV_SIMPLIFIED,
    CDHV_TRADITIONAL
};

struct ucd_han_variant {
    enum ucd_han_variant_type type;
    uint32_t code_point;
}

struct ucd_han_variants {
    struct ucd_han_variant variant[4];
}

struct ucd_data {
    uint32_t code_point;
    enum ucd_category general_category;
    uint8_t combining_class;
    struct ucd_decomposition *decomposition;
    uint32_t uppercase_mapping;
    uint32_t lowercase_mapping;
    uint32_t titlecase_mapping;
    struct ucd_han_variants *variants;
};

struct ucd_sparse_run {
    uint32_t first;
    uint32_t last;
    size_t offset;
};

#define UCD_BMP_LENGTH 0x10000
#define UCD_SPARSE_CODE_POINTS_LENGTH 15035
#define UCD_SPARSE_RUNS_LENGTH 293
#define UCD_DECOMPOSITIONS_LENGTH 2060

struct ucd_data ucd_bmp[UCD_BMP_LENGTH];
struct ucd_data ucd_sparse_code_points[UCD_SPARSE_CODE_POINTS_LENGTH];
struct ucd_sparse_run ucd_sparse_runs[UCD_SPARSE_RUNS_LENGTH];
struct ucd_decomposition ucd_decompositions[UCD_DECOMPOSITIONS_LENGTH];
