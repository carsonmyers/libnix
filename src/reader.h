#ifndef INCLUDE_reader_h__
#define INCLUDE_reader_h__

#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>

#include "libnix/reader.h"

#include "common.h"
#include "decode.h"

const size_t READER_BUFFER_INIT_SIZE = 1024 * 1024; // 1 MiB

enum reader_type {
    NIX_FILE_READER,
    NIX_CSTRING_READER,
    NIX_MEM_READER
};

struct reader {
    // For casting into subtypes
    enum reader_type reader_type;

    // Buffer for chunked reading
    uint8_t *buffer;
    uint8_t *read;
    size_t buffer_size;
    size_t data_length;

    // Decomposition state information
    enum nix_err (*decoder)(struct reader *, uint8_t *, size_t);
    struct nix_decode_state decode_state;

    bool input_exhausted;
};

struct file_reader {
    struct reader p;
    FILE *in;
};

struct cstring_reader {
    struct reader p;
    char *in;
};

struct mem_reader {
    struct reader p;
    int length;
    uint8_t *in;
};

enum nix_err
nix_reader__init_file(
    struct file_reader *out,
    FILE *in,
    enum nix_encoding encoding
);

enum nix_err
nix_reader__init_cstring(
    struct cstring_reader *out,
    char *in,
    enum nix_encoding encoding
);

enum nix_err
nix_reader__init_mem(
    struct mem_reader *out,
    uint8_t *in,
    int length,
    enum nix_encoding encoding
);

enum nix_err
__read_file(
    struct file_reader *reader,
    uint8_t *out,
    size_t *num_read,
    size_t count
);

enum nix_err
__read_cstring(
    struct cstring_reader *reader,
    uint8_t *out,
    size_t *num_read,
    size_t count
);

enum nix_err
__read_mem(
    struct mem_reader *reader,
    uint8_t *out,
    size_t *num_read,
    size_t count
);

enum nix_err
__load_file(struct file_reader *reader);

enum nix_err
__load_cstring(struct cstring_reader *reader);

enum nix_err
__load_mem(struct mem_reader *reader);

#endif
