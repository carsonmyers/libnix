#include <stdlib.h>
#include <string.h>

#ifndef INCLUDE_common_h__
#define INCLUDE_common_h__

#include "libnix/common.h"

#include "error.h"

#define ALLOC(dst, size) if (!(dst = malloc(size))) { \
    __nix_local_err = NIXERR_NOMEMORY; \
        goto except; \
    } else { \
        memset(dst, 0, size); \
    }

#define FREE(ptr) if (ptr != NULL) { \
        free((void *)ptr); \
    }

#endif
