#ifndef INCLUDE_file_reader_h__
#define INCLUDE_file_reader_h__

#include <stdio.h>

#include "libnix/file_reader.h"

struct file_reader {
    FILE *in;
    uint8_t *buffer;
    uint8_t *read;

    enum nix_err (*encode)(struct file_reader *, uint8_t *, int);
};

enum nix_err
nix_buffer__init(struct file_reader *out, FILE *in);

enum nix_err
__encode_utf8(struct file_reader *reader, uint8_t *dest, int count);

enum nix_err
__encode_utf16_le(struct file_reader *reader, uint8_t *dest, int count);

enum nix_err
__encode_utf16_be(struct file_reader *reader, uint8_t *dest, int count);

enum nix_err
__encode_utf32_le(struct file_reader *reader, uint8_t *dest, int count);

enum nix_err
__encode_utf32_be(struct file_reader *reader, uint8_t *dest, int count);

#endif
