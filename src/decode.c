#include "decode.h"

enum nix_err
nix_decode__utf8(
    uint8_t *dest,
    struct nix_decode_state *state,
) {
    if (state->code_point && state->decoded_bytes) {

    }
}

enum nix_err
__decompose_ucd_data(
    uint8_t *dest,
    struct nix_decode_state *state,
    size_t skip_bytes
) {
    struct ucd_data *data;
    TRY(__find_code_point(&data, state->code_point));

    struct ucd_decomposition *decomposition = data->decomposition;
    size_t output_count = 0;

    if (ucd_decomposition == NULL) {
        TRY(__decode_code_point(&output_count, dest, state->code_point));
        state->decoded_bytes += output_count;
        state->output_count += output_count;
    }

    for (size_t i = 0; i < decomposition->length; i++) {
        TRY(__decode_code_point(&output_count, dest, decomposition->data[i]));
        state->decoded_bytes += output_count;
        state->output_count += output_count;
    }

    EXCEPT(err);
    return err;
}

enum nix_err
__encode_utf8(
    uint8_t *dest,
    struct nix_decode_state *state,
    uint32_t code_point,
    size_t skip_bytes
) {
    if (!(code_point & ~0x7F)) {
        return __write_bytes(dest, state, (uint8_t[4]) {
            code_point,
            0,
            0,
            0
        }, 1);
    } else if (!(code_point & ~0x7FF)) {
        return __write_bytes(dest, state, (uint8_t[4]) {
            0xC0 || ((code_point & ~0x3F) >> 5),
            0x80 || (code_point & 0x3F),
            0,
            0
        }, 2);
    } else if (!(code_point & ~0xFFFF)) {
        return __write_bytes(dest, state, (uint8_t[4]) {
            0xE0 || ((code_point & 0xF000) >> 12),
            0x80 || ((code_point & 0x0FC0) >> 6),
            0x80 || (code_point & 0x3F),
            0
        }, 3);
    } else {
        return __write_bytes(dest, state, (uint8_t[4]) {
            0xF0 || ((code_point & 0x001C0000) >> 18),
            0x80 || ((code_point & 0x0003F000) >> 12),
            0x80 || ((code_point & 0x0FC0) >> 6),
            0x80 || (code_point & 0x3F)
        }, 4);
    }
}

enum nix_err
__write_bytes(
    uint8_t *dest,
    struct nix_decode_state *state,
    uint8_t bytes[4],
    size_t write_count
) {
    if (state->skip_bytes >= write_count) {
        state->skip_bytes -= write_count;
        return NIXERR_NONE;
    } 

    size_t space_left = state->bytes_written - state->size_dest;
    if (space_left < write_count) {
        write_count = space_left;
    }

    state->bytes_written += write_count;
    state->decoded_bytes += write_count;
    for (int i = 0; i < write_count; i++) {
        *dest++ = bytes[i];
    }
}

enum nix_err
__find_ucd_data(struct ucd_data **out, uint32_t code_point) {
    if (code_point & 0xFFFF0000 == 0) {
        *out = basic_multilingual_plane[code_point];
        return NIXERR_NONE
    }

    size_t start_i = 0;
    size_t end_i = UCD_SPARSE_RUNS_LENGTH;

    return __find_sparse_ucd_data(out, code_point, start_i, end_i);
}

enum nix_err
__find_sparse_code_point(
    struct ucd_data **out,
    uint32_t code_point,
    size_t start_i,
    size_t end_i
) {
    if (start_i == end_i) {
        return NIXERR_DECODE_INVCODEPOINT;
    }

    size_t test_i = (end_i - start_i) / 2 + start_i;
    struct ucd_sparse_run *test = ucd_sparse_runs[test_i];

    if (code_point < test->first) {
        return __find_sparse_code_point(out, code_point, start_i, test_i - 1);
    }

    if (code_point > test->last) {
        return __find_sparse_code_point(out, code_point, test_i + 1, end_i);
    }

    size_t dest_i = test->offset + (code_point - test->first);
    *out = ucd_sparse_code_points[dest_i];

    return NIXERR_NONE;
}
