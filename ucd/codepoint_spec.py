"""Specification for a range of codepoints
"""
import re

PATTERN = re.compile(r"""
(
# UCD machine, e.g. `01AF`, `01AF..0200`
    (?P<start_ucd>
        [0-9a-fA-F]{4,}
    )
    (?:
        \.\.
        (?P<end_ucd>
            [0-9a-fA-F]{4,}
        )?
    )?

# Unihan machine, e.g. `U+01AF`
) | (
    U\+
    (?P<start_unihan>
        [0-9a-fA-F]{4,}
    )
)
""", re.VERBOSE)


class CodepointSpec:
    """Iterator-based specification for a range of one or more codepoints

    Recognizes UCD and Unihan style codepoints, as well as UCD ranges.
    Can be iterated over (even a single codepoint) and compared for equality
    """
    def __init__(self, raw):
        match = PATTERN.match(raw)
        if match is None:
            raise ValueError(
                "\"{}\" is not a valid codepoint range".format(raw)
            )

        start_spec = match.group("start_ucd") or match.group("start_unihan")
        end_spec = match.group("end_ucd")

        self.start = int(start_spec, 16)
        if end_spec is None:
            self.end = self.start
        else:
            self.end = int(end_spec, 16)

    def __iter__(self):
        return iter(range(self.start, self.end + 1))

    def __eq__(self, other):
        if isinstance(other, int):
            return self.start == self.end and self.start == other
        elif isinstance(other, CodepointSpec):
            return self.start == other.start and self.end == other.end

        return False

    def __lt__(self, other):
        if isinstance(other, int):
            return self.start < other
        elif isinstance(other, CodepointSpec):
            if self.start < other.start:
                return True
            if self.start == other.start and self.end < other.end:
                return True

        return False

    def __le__(self, other):
        return self < other or self == other

    def __gt__(self, other):
        if isinstance(other, int):
            return self.start > other
        elif isinstance(other, CodepointSpec):
            if self.start > other.start:
                return True
            if self.start == other.start and self.end > other.end:
                return True

        return False

    def __ge__(self, other):
        return self > other or self == other

    def __contains__(self, other):
        if isinstance(other, int):
            return self.start <= other <= self.end
        elif isinstance(other, CodepointSpec):
            return self.start <= other.start and other.end <= self.end

        return False

    def __str__(self):
        if self.start == self.end:
            return "0x{:X}".format(self.start)
        else:
            return "0x{:X} ⇒ 0x{:X}".format(self.start, self.end)

    def __int__(self):
        if self.start != self.end:
            raise ValueError("Cannot interpret ranged codepoint as int")
        else:
            return self.start
