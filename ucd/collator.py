"""Unicode Database table collator

For reading and combining entries from across multiple tables
"""
from ucd.file_reader import FileReader

class CollatorIterator:
    """Iterator for `Collator`

    Moves over multiple unicode database tables collating entries from all of
    them, accounting for gaps and ranges.
    """
    def __init__(self, *reader_iterators):
        self.__iterators = map(iter, reader_iterators)
        self.__current_indices = {}
        self.__past_indices = {}

        # When initializing the iterator, prime the state with the first
        # value of each file iterator
        for iterator in self.__iterators:
            value = CollatorIterator.__get_next_value(iterator)
            self.__current_indices[iterator.name] = \
                (value["index"], value["fields"], iterator)

    def __next__(self):
        # Find the next lowest codepoint in all files
        existing = filter(None, self.__current_indices.values())
        indices = map(lambda x: x[0], existing)

        # If all codepoints are `None`, then all the files have been
        # exhausted and iteration is finished
        try:
            current_index = min(indices)
        except ValueError:
            raise StopIteration

        result = {}

        for source, item in self.__current_indices.items():
            # Skip exhausted files
            if item is None:
                result[source] = None
                continue

            index, fields, iterator = item

            # Only process the current codepoint (the furthest behind iterator
            # needs to catch up to those farther ahead)
            if index == current_index:
                result[source] = fields

                # Update the state for this iterator
                new_value = CollatorIterator.__get_next_value(iterator)
                if new_value is None:
                    self.__current_indices[source] = None
                else:
                    self.__current_indices[source] = (
                        new_value["index"],
                        new_value["fields"],
                        iterator)
            else:
                result[source] = None

        return {
            "index": current_index,
            "fields": result,
        }

    @staticmethod
    def __get_next_value(iterator):
        try:
            value = next(iterator)
            return value
        except StopIteration:
            return None

class Collator:
    """Collator for `FileReader`s

    Multiple tables can be read at once, iterating over one codepoint at a
    time and accounting for misaligned files, gaps in entries, and ranges
    of entries.

    `sources` should be dictionaries with the following structure:

    ```
    {
        "filename": "ucd/database/example.txt",
        "options": ucd.entry_reader.EntryReaderOptions(),
    }
    ```

    where `filename` is the database file, and `options` is an
    `EntryReaderOptions` instance to control the entry reader for that
    particular file.
    """
    def __init__(self, *sources):
        self.__file_readers = map(Collator.make_reader, sources)

    @staticmethod
    def make_reader(source):
        """Helper method to create a file reader from the source input
        """
        return FileReader(source["filename"], source["options"])

    def __iter__(self):
        return CollatorIterator(*self.__file_readers)
