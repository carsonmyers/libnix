"""Reader for UCD and Unihan entries

Iterator-based reader for unicode database tables. Used to implement
FileReaderIterator
"""
import re
from ucd.codepoint_spec import CodepointSpec
from ucd.errors import (
    UnexpectedCodepointRangeBeginError,
    UnexpectedCodepointRangeEndError,
    ExpectedEndOfCodepointRangeError,
    InvalidRangeCodepointError,
)

REPEATING_NAME_PATTERN = re.compile(r"""
    ^<(?P<name>[^,]+), \s (?P<side>First|Last)>$
""", re.VERBOSE)


class EntryReaderOptions:
    """Base EntryReader options class

    Contains default options and can be extended for convenience

    Options:
    - `delimiter`: A compiled regex used to split a data line
    - `allow_multiples`: Whether a codepoint can have multiple (contiguous)
        entries in the source file. (all sets of fields are returned in
        an array)
    - `detect_pairs`: Whether to detect ranges expressed as First/Last pairs
        of entries denoted in their `name` field, such as those in
        `UnicodeData.txt`
    - `index_field`: Integer index of the indexing field; Used for ordering
        and grouping entries. Usually `0`
    - `index_map`: Function for pre-processing each index field; Usually
        this is `CodepointSpec` but if the index is a naturally
        ordered value then this can just be an identity function
    - `name_field`: Integer index of the name field; required for `detect_pairs`
    - `out_of_order`: Entries can appear out of order; the file is read and
        the entries are sorted ahead of time
    """
    delimiter = re.compile(r";")
    allow_multiples = False
    detect_pairs = False
    index_field = 0
    index_map = CodepointSpec
    name_field = None
    out_of_order = False

    def __init__(self, **kv):
        for key, value in kv.items():
            self.__setattr__(key, value)

        if self.detect_pairs and self.name_field is None:
            raise ValueError("name_field required with detect_pairs")

    def __str__(self):
        return "mul: {}; pair: {}; name: {}; index: {}, ooo: {}".format(
            self.allow_multiples, self.detect_pairs, self.index_field,
            self.name_field, self.out_of_order)


class UcdEntryReaderOptions(EntryReaderOptions):
    """Base options for UCD EntryReaders

    UCD tables use the ; separator and don't support multiple entries per
    codepoint; they do support codepoint ranges, though.

    Use detect_pairs and name_field=1 for UnicodeData.txt, as it contains
    ranges noted by pairs of First/Last entries.
    """
    def __init__(self, **kv):
        super(UcdEntryReaderOptions, self).__init__(**kv)


class UnihanEntryReaderOptions(EntryReaderOptions):
    """Base options for Unihan EntryReaders

    Unihan tables are space-separated and support multiple entries per
    codepoint. Codepoint ranges are not used.
    """
    def __init__(self, **kv):
        options = {
            "delimiter": re.compile(r"\s+"),
            "allow_multiples": True,
        }

        options.update(kv)

        super(UnihanEntryReaderOptions, self).__init__(**options)


class AliasEntryReaderOptions(EntryReaderOptions):
    """Base options for alias EntryReaders

    Alias files are metadata and don't give any details about codepoints.
    The default inde_map (which creates a CodepointSpec) would fail, because
    the indexes are various short names for Unicode concepts.
    """
    def __init__(self, **kv):
        options = {
            "allow_multiples": True,
            "index_map": lambda x: [x],
        }

        options.update(kv)

        super(AliasEntryReaderOptions, self).__init__(**options)


class EntryReaderIterator:
    """Iterator for EntryReader

    Reads entries from an input file-like object (with readline support).

    An entry may span multiple lines and multiple codepoints. Non data lines
    (comments and blank lines) are skipped. Configure the behaviour of this
    iterator with `EntryReaderOptions` and its subclasses.
    """
    def __init__(self, data_in, options):
        self.input = data_in
        self.options = options
        self.__next_entry = None
        self.__all_entries = None

        if options.out_of_order:
            entries = []
            while True:
                try:
                    index, fields = self.__load_single_entry(False)
                except StopIteration:
                    break

                self.__sorted_insert(entries, index, fields)

            self.__all_entries = iter(entries)

    def __next__(self):
        # Skip multiple detection if not enabled
        if not self.options.allow_multiples:
            return self.__load_single_entry()

        # An entry was already loaded but not returned by the last call
        if self.__next_entry is not None:
            index, fields = self.__next_entry
            entries = [fields]
            self.__next_entry = None
        else:
            index = None
            entries = []

        while True:
            try:
                next_index, fields = self.__load_single_entry()
            except StopIteration:
                if index is None:
                    raise

                break

            # First entry: set the codepoint and first set of fields
            if index is None:
                index = next_index
                entries.append(fields)
                continue

            # Found a multiple: aggregate its fields
            elif next_index == index:
                entries.append(fields)

            # New codepoint - store this entry for the next call
            else:
                self.__next_entry = next_index, fields
                break

        # With `detect_multiples`, entries will always be returned as an
        # array-of-arrays, even if no multiples are found
        return index, entries

    def __load_single_entry(self, trim_fields=True):
        """Load a single entry, despite `detect_multiples` setting

        Multiple "entries" may still be consumed in the case of entry-pair
        codepoint ranges, but that is just another way to express a range as
        the ranges are otherwise identical.
        """
        if self.options.out_of_order and self.__all_entries is not None:
            index, fields = next(self.__all_entries)
        else:
            fields = self.__read_line()
            index = self.options.index_map(fields[self.options.index_field])

        if self.options.detect_pairs:
            name = fields[self.options.name_field]
            repeat_spec = EntryReaderIterator.__get_repeat_spec(name)
            if repeat_spec is not None:
                entry_range = self.__load_entry_range(repeat_spec, fields)
                index = iter(range(entry_range[0], entry_range[1] + 1))
                fields[self.options.name_field] = repeat_spec["name"]

        if trim_fields:
            start = fields[:self.options.index_field]
            end = fields[self.options.index_field + 1:]

            fields = start + end

        return index, fields

    def __read_line(self):
        """Read and split a single data-line

        Non-data-lines (empty lines or comments) are skipped until a data-line
        can be found, or until the file is exhausted.
        """
        line = None
        while True:
            raw = self.input.readline()
            if not raw:
                raise StopIteration()

            line = raw.split("#", maxsplit=1)[0].strip()
            if not line:
                continue

            break

        return [f.strip() for f in self.options.delimiter.split(line)]

    def __load_entry_range(self, spec_start, fields_start):
        """Find the codepoint range from an entry-pair range

        The beginning of the range is already found and represented by
        `spec_start` and `fields_start` - this method will load the end of
        the range and ensure it is well-formed before returning the first
        and last (inclusive) codepoints in the range.
        """
        index_start = fields_start[self.options.index_field]

        try:
            index_int_start = int(self.options.index_map(index_start))
        except ValueError:
            raise InvalidRangeCodepointError(
                index_start, fields_start[self.options.name_field])

        # Range must start with the "First" entry
        if spec_start["side"] != "First":
            raise UnexpectedCodepointRangeEndError(
                index_int_start, spec_start["name"])

        fields_end = self.__read_line()
        index_end = fields_end[self.options.index_field]

        try:
            index_int_end = int(self.options.index_map(index_end))
        except ValueError:
            raise InvalidRangeCodepointError(
                index_end, fields_end[self.options.name_field])

        spec_end = EntryReaderIterator.__get_repeat_spec(
            fields_end[self.options.name_field])

        # The end of the range must be the next entry from the beginning
        if spec_end is None:
            raise ExpectedEndOfCodepointRangeError(
                index_end, fields_end[self.options.name_field])

        # Range must end with the "Last" entry
        if spec_end["side"] != "Last":
            raise UnexpectedCodepointRangeBeginError(
                index_end, spec_end["name"])

        # Range names must match
        if spec_end["name"] != spec_start["name"]:
            raise UnexpectedCodepointRangeEndError(
                index_end, spec_end["name"])

        return index_int_start, index_int_end

    def __sorted_insert(self, target, payload_index, payload):
        for i, entry in enumerate(target):
            if entry[0] > payload_index:
                target.insert(i, (payload_index, payload))
                return

        target.append((payload_index, payload))

    @staticmethod
    def __get_repeat_spec(name):
        """Try to extract range information from a codepoint name

        An entry in an entry-pair range will have a name that looks like
        "<Name, First>" - this method extracts the name and side (either
        "First" or "Last") from the name field, or returns None.
        """
        match = REPEATING_NAME_PATTERN.match(name)
        if match is None:
            return None

        return {
            "name": match.group("name"),
            "side": match.group("side"),
        }

class EntryReader:
    """Reader for UCD entries in file-like objects

    Used to implement `FileReader` without caring about filenames, etc.
    Use `EntryReaderOptions` and its subclasses to configure reader behaviour.
    """
    def __init__(self, data_in, options):
        self.input = data_in
        self.options = options

    def __iter__(self):
        return EntryReaderIterator(self.input, self.options)
