"""Decomposition of one codepoint into several
"""
import re

class CharacterDecomposition:
    """Unicode character decomposition

    Supports canonical decompositions as well as all other types present in
    `UnicodeData.txt`. Other settings should determine which of these are
    passed along to the source template.
    """
    __PATTERN = re.compile(r"""
    # Optional type parameter
    (?:
        <
        (?P<type>
            \w+
        )
        >\s
    )?

    # Space-separated list of code-points
    (?P<code_points>
        (?:
            [0-9A-F]{4,}\s?
        )+
    )
    """, re.VERBOSE)

    def __init__(self, raw):
        match = CharacterDecomposition.__PATTERN.match(raw)

        if match is None:
            raise ValueError("Invalid decomposition \"{}\"".format(raw))
        else:
            self.type = match.group("type")
            # Canonical decompositions are listed without a type
            if self.type is None:
                self.type = "canonical"

            parse = lambda c: int(c, 16)
            items = match.group("code_points").split()
            self.code_points = list(map(parse, items))

    def __str__(self):
        codepoints = map("0x{:X}".format, self.code_points)
        return "‹{}› {}".format(self.type, " ".join(codepoints))
