"""All project exception classes
"""

class InvalidDownloadPathError(Exception):
    """Only zip archives can be downloaded
    """
    def __init__(self, target_path):
        super(InvalidDownloadPathError, self).__init__(
            "Download target path {} doesn't look like a zip file"
            .format(target_path)
        )


class DuplicateTargetError(Exception):
    """Archives of the same name would conflict in the same target directory
    """
    def __init__(self, basename, target_path, other_target):
        super(DuplicateTargetError, self).__init__(
            "Downloading of {} failed: {} will already be saved to {}"
            .format(target_path, basename, other_target)
        )


class UnsupportedUrlSchemeError(Exception):
    """Only HTTPS and FTP are supported
    """
    def __init__(self, scheme, url):
        super(UnsupportedUrlSchemeError, self).__init__(
            "Unsupported target scheme \"{}\": {}"
            .format(scheme, url)
        )


class FTPNetworkError(Exception):
    """General error wrapper for FTP errors
    """
    def __init__(self, url):
        super(FTPNetworkError, self).__init__(
            "FTP Connection failed for \"{}\""
            .format(url)
        )


class UnexpectedCodepointRangeBeginError(Exception):
    """The beginning of an entry-pair range was not expected
    """
    def __init__(self, codepoint, name):
        super(UnexpectedCodepointRangeBeginError, self).__init__(
            "Unexpected start of codepoint range at 0x{:X} ({})"
            .format(codepoint, name)
        )


class UnexpectedCodepointRangeEndError(Exception):
    """The end of an entry-pair range was not expected
    """
    def __init__(self, codepoint, name):
        super(UnexpectedCodepointRangeEndError, self).__init__(
            "Unexpected end of codepoint range at 0x{:X} ({})"
            .format(codepoint, name)
        )


class ExpectedEndOfCodepointRangeError(Exception):
    """The end of an entry-pair range was not found
    """
    def __init__(self, codepoint, name):
        super(ExpectedEndOfCodepointRangeError, self).__init__(
            "Expected end of range at 0x{:X}, found \"{}\" instead"
            .format(codepoint, name)
        )


class InvalidRangeCodepointError(Exception):
    """Entry-pair range specifications must be simple hexadecimal numbers
    """
    def __init__(self, codepoint, name):
        super(InvalidRangeCodepointError, self).__init__(
            "Invalid codepoint in database: {} ({})"
            .format(codepoint, name)
        )
