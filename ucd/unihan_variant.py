"""Processing for unihan variants
"""
from ucd.codepoint_spec import CodepointSpec

class UnihanVariant:
    """Processes unihan variants and their sources

    Variants include Z variants (essentially font differences), semantic
    variants (ideographs with similar etymology - these are sourced from
    various dictionaries), and simplified/traditional variants.
    """
    def __init__(self, fields, meta):
        self.meta = meta
        self.type = meta.unihan_variant_types.get_index(fields[0])
        self.variants = map(self.extract_variant, fields[1:])

    def extract_variant(self, raw):
        """Separate the codepoint from the sources if present

        A variant is specified as:

            U+6191<kLau,kMatthews

        with the `<` separating the codepoint from the sources, and `,`
        delimiting the source list.
        """
        parts = raw.split("<")
        codepoint_raw = parts[0]
        source_list = parts[1].split(",") if len(parts) == 2 else []

        codepoint = CodepointSpec(codepoint_raw).start

        sources = map(
            self.meta.unihan_variant_sources.get_index, source_list)

        return {
            "codepoint": codepoint,
            "sources": list(sources),
        }

    def __str__(self):
        parts = [
            "‹{}› ".format(self.meta.unihan_variant_types.get_string(self.type))
        ]

        variants = []
        for variant in self.variants:
            variant_parts = []
            variant_parts.append("0x{:X}".format(variant["codepoint"]))

            if variant["sources"]:
                sources = map(
                    self.meta.unihan_variant_sources.get_string,
                    variant["sources"])

                variant_parts.append(" ({})".format(", ".join(sources)))

            variants.append("".join(variant_parts))

        parts.append(", ".join(variants))

        return "".join(parts)
