"""Read UCD entries from files

Iterator-based file-reader for unicode database tables.
"""
import itertools
import os
from ucd.entry_reader import EntryReader, EntryReaderOptions

class FileIterator:
    """Iterator for FileReader

    use `ucd.entry_readers.EntryReaderOptions` and its subclasses to configure
    the entry reader per file (as they vary in format)
    """
    def __init__(self, filename, reader_options=None):
        if reader_options is None:
            reader_options = EntryReaderOptions()

        self.filename = filename
        self.name = os.path.splitext(os.path.basename(filename))[0]
        self.file = open(filename, "r", encoding="utf-8")
        self.__reader = iter(EntryReader(self.file, reader_options))
        self.__index_iter = None
        self.__fields = None

    def __next__(self):
        # First run: load the first entry
        if self.__index_iter is None:
            self.__load_next_entry()
            return next(self)

        # Each entry might have a range of codepoints, so repeat the entry
        # with every element in the range until it is exhausted 
        try:
            index = next(self.__index_iter)

            return {
                "index": index,
                "fields": self.__fields,
            }
        except StopIteration:
            # Range was exhausted, load the next entry
            self.__load_next_entry()
            return next(self)

    def __load_next_entry(self):
        index, fields = next(self.__reader)
        self.__index_iter = iter(index)
        self.__fields = fields


class FileReader:
    """Iterator-based file-reader for unicode database tables

    Use `ucd.entry_readers.EntryReaderOptions` and its subclasses to configure
    the entry reader per file (as they vary in format)
    """
    def __init__(self, filename, reader_options=None):
        if reader_options is None:
            reader_options = EntryReaderOptions()

        self.filename = filename
        self.name = os.path.splitext(os.path.basename(filename))[0]
        self.reader_options = reader_options

    def __iter__(self):
        return FileIterator(self.filename, self.reader_options)
