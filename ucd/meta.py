"""Classes for collecting and indexing properties and their values and aliases.

Property aliases add short names and aliases as indexes to the meta object,
and property value aliases create enumerations of values which can be indexed
by codepoint entries (rather than copying value data into large numbers of
codepoints).
"""
from ucd.codepoint_spec import CodepointSpec


class PropertyValueAliases:
    """Property value alias aggregator

    An object can be initialized with aliases taken from
    PropertyValueAliases.txt - loaded from the meta dict which is loaded
    before the meta class and the UCD files.

    Otherwise, entries and indexes are created as they occur in a database
    file - meaning that the values of the indexes themselves may not be
    stable across Unicode versions and depend upon their order of use.
    """
    def __init__(self, meta_category=None):
        self.properties = []
        self.indices = {}

        if meta_category is None:
            return

        for meta in meta_category:
            short_name = meta[0]
            aliases = list(meta[1:])
            self.__insert_property(short_name, aliases)

    def get_index(self, short_name):
        """Get the index corresponding to the short name of a value.

        When aliases have been preloaded, this index will correspond both
        to the short name and any aliases.
        """
        if short_name in self.indices:
            return self.indices[short_name]

        return self.__insert_property(short_name, [])

    def get_string(self, index):
        """Get the string representation of a meta index

        This is used for logging and debugging, and so a human-readable
        combination of the short name and a list of aliases is generated.
        """
        if len(self.properties) < index:
            raise IndexError("Invalid property index {}".format(index))

        prop = self.properties[index]
        if not prop["aliases"] == 0:
            return prop["short_name"]

        return "{} ‹{}›".format(prop["short_name"], ", ".join(prop["aliases"]))

    def __create_property(self, short_name):
        return {
            "short_name": short_name,
            "aliases": [],
            "indices": [],
        }

    def __insert_property(self, short_name, aliases):
        # The short name has already been seen, and its existing index
        # will be used
        if short_name in self.indices:
            index = self.indices[short_name]
            prop = self.properties[index]

        # This is a new short name, no aliases were loaded for it, and a new
        # index will be created
        else:
            prop = self.__create_property(short_name)
            index = len(self.properties)
            self.indices[short_name] = index
            self.properties.append(prop)

        # Extend the aliases rather than replacing them; this is useful for
        # nested values (general categories) where an entry may have been
        # created for a parent value before its aliases have been loaded
        prop["aliases"].extend(aliases)
        for alias in aliases:
            self.indices[alias] = index

        # Can be extended to support nested properties (e.g. general
        # categories), but is a no-op by default.
        self.__insert_property_recursive(index, prop)

        return index

    def __insert_property_recursive(self, index, prop):
        return


class GeneralCategoryAliases(PropertyValueAliases):
    """Property value specialization for general categories

    General categories are nested (e.g. L, Lu, Lc, Lo, etc.), so this class
    implements the recursive insertion behaviour which puts all property
    values in the flat property list, while also collecting child indices.
    """
    def __insert_property_recursive(self, index, prop):
        # Single-character categories are the root level
        if len(prop["short_name"]) <= 1:
            return

        # Multi-character categories are nested (e.g., Lu is a child of L).
        # It must be ensured that the parent code exists in the properties
        # list so that the child index can be added.
        parent_code = prop["short_name"][:-1]
        parent_index = self.__insert_property(parent_code, [])

        self.properties[parent_index]["indices"].append(index)


class Meta:
    """Metadata for indexing property values

    Many properties have short names and aliases, and a limited number
    of possibilities which can be enumerated as indices. Storing the values
    and aliases in a table and indices in the codepoint data reduces the space
    needed to store a codepoint entry.
    """
    def __init__(self, meta):
        self.bidi_class = PropertyValueAliases(meta["Bidi_Class"])
        self.blocks = PropertyValueAliases()
        self.case_folding = PropertyValueAliases()
        self.combining_categories = PropertyValueAliases(
            meta["Canonical_Combining_Class"])
        self.general_categories = GeneralCategoryAliases(
            meta["General_Category"])
        self.hangul_syllable_types = PropertyValueAliases(
            meta["Hangul_Syllable_Type"])
        self.line_breaks = PropertyValueAliases(meta["Line_Break"])
        self.scripts = PropertyValueAliases(meta["Script"])
        self.special_casing = PropertyValueAliases()
        self.unihan_numeric_values = PropertyValueAliases()
        self.unihan_variant_types = PropertyValueAliases()
        self.unihan_variant_sources = PropertyValueAliases()

        self.init_line_break_defaults()

    def init_line_break_defaults(self):
        self.line_break_id_blocks = map(self.blocks.get_index, [
            "CJK Unified Ideographs Extension A",
            "CJK Unified Ideographs",
            "CJK Compatibility Ideographs",
        ])

        self.line_break_id_ranges = [
            CodepointSpec("U+20000..U+2FFFD"),
            CodepointSpec("U+30000..U+3FFFD"),
            CodepointSpec("U+1F000..U+1FFFD"),
        ]

        self.line_break_pr_block = self.blocks.get_index("Currency Symbols")