#include <stdint.h>
#include <stdlib.h>

#include "ucd.h"

struct ucd_data ucd_bmp[$bmp_length] = {
$bmp_entries
};

struct ucd_data ucd_sparse_code_points[$sparse_code_points_length] = {
$sparse_code_points_entries
};

struct ucd_sparse_run ucd_sparse_runs[$sparse_runs_length] = {
$sparse_runs_entries
};

struct ucd_decomposition ucd_decompositions[$decompositions_length] = {
$decompositions_entries
};
