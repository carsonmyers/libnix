"""Helper functions for decoding values from unicode database files
"""
from typing import List

def decode_boolean(value: str) -> bool:
    """Decode a boolean value

    Boolean values in UCD files can be T, F, True, No, etc.
    These values can be loaded programmatically from PropertyValueAliases.txt,
    but they occur frequently and always with the same values.

    It's also convenient to explicitly write boolean values to the output,
    rather than indexing into a boolean values table (where index 0 might
    correspond to False, and 1 to True, but that's not necessarily the case -
    and testing the values in the output would require a pointer dereference)
    """
    if value in ("T", "True", "Y", "Yes"):
        return True

    if value in ("F", "False", "N", "No"):
        return False

    raise ValueError("Invalid boolean value: {}".format(value))


def decode_codepoint(value: str) -> int:
    """Decode a codepoint

    This is not the same as CodepointSpec - only actual numerical values can
    be decoded this way, where CodepointSpec includes various codepoint formats
    and accommodates ranges.

    Values in the UCD are simple numbers, or strings of numbers.
    """
    if not value:
        return None

    try:
        return int(value, 16)
    except ValueError:
        raise ValueError("{} is not a valid codepoint value")


def decode_codepoint_sequence(value: str) -> List[int]:
    """Decode a sequence of codepoints

    Codepoints often appear in space-delimited sequences in UCD files. This
    just splits them apart and uses `decode_codepoint` on the components
    """
    if not value:
        return None

    codepoints = value.split()
    return list(map(decode_codepoint, codepoints))
