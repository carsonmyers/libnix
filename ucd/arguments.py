"""Command-line arguments for UCD source-file generation

Includes argparse arguments and helper functions for building a command-line
tool to generate UCD source files.
"""
import argparse

__ARGUMENTS = [{
    "name": ["-u", "--update-ucd"],
    "props": {
        "help": "Download a copy of the unicode character database",
        "action": "store_true",
    }
}, {
    "name": ["-s", "--ucd-source"],
    "props": {
        "help": "Source URL for the UCD archives (ftp or https)",
        "default": "ftp://www.unicode.org/Public/UCD/latest",
    }
}, {
    "name": ["-A", "--ucd-archives"],
    "props": {
        "action": "append",
        "help": "Archives to download and extract from the source",
        "default": [],
    }
}, {
    "name": ["-d", "--ucd-archive-destination"],
    "props": {
        "help": "Download target for UCD archives",
        "default": "./ucd/archive",
    }
}, {
    "name": ["-D", "--ucd-data-destination"],
    "props": {
        "help": "Extraction target for UCD files",
        "default": "./ucd/database",
    }
}, {
    "name": ["-C", "--c-dist"],
    "props": {
        "help": "Output target for C source files",
        "default": "./src",
    }
}, {
    "name": ["-H", "--h-dist"],
    "props": {
        "help": "Output target for header files",
        "default": "./src",
    }
}, {
    "name": ["--test"],
    "props": {
        "action": "store_true",
        "help": "Run database tests",
        "default": False,
    }
}]

def flatten_argument_list(arg_list):
    """Combine multiply-specified list args with comma-delimited arguments

    List argument could be specified like:

    - `-arg=value1,value2,value3`
    - `-arg value1 -arg value2 -arg value3`

    or a combination of the two. This will create a flat list of all
    specified options
    """
    listified = [arg.split(",") for arg in arg_list]
    return [item for sublist in listified for item in sublist]

def parse_args(raw_args):
    """Create an argparser and parse argument input
    """
    parser = argparse.ArgumentParser()
    for arg in __ARGUMENTS:
        parser.add_argument(*arg["name"], **arg["props"])

    args = parser.parse_args(raw_args)

    if not args.ucd_archives:
        args.ucd_archives = [
            "ucd/UCD.zip",
            "ucd/Unihan.zip"
        ]
    else:
        args.ucd_archives = flatten_argument_list(args.ucd_archives)

    return args
