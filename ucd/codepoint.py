"""Contains codepoint class for collating UCD values for a single codepoint.
"""
from fractions import Fraction
from ucd.character_decomposition import CharacterDecomposition
from ucd.decode import (
    decode_boolean,
    decode_codepoint,
    decode_codepoint_sequence,
)
from ucd.unihan_variant import UnihanVariant

class Codepoint:
    """Collection of UCD values for a single codepoint.

    Values are decoded from strings into indexes into meta collections,
    numbers, boolean values, etc. Values collated from all input database
    files are collected and decoded here - although not all data from
    every file is used.

    A meta index is an index into a list of meta values which were collected
    as codepoints were read, or when the PropertyValueAliases.txt file was
    processed. This allows metadata to be collected into one place, and only
    the index stored in the codepoint

    Values:
    - `name` (string): Name of the codepoint (e.g. LATIN CAPITAL LETTER A)
    - `category` (meta index): General category of the codepoint (e.g. Lu)
    - `combining_class` (int): Numeric class describing the combining behaviour
        of a codepoint. The metadata is not used, as this value will be used
        mainly for column index detection and normalization
    - `bidi_class` (meta index): Bidirectional behaviour class (e.g. ARABIC)
    - `bidi_mirrored` (bool): Whether a character is mirrored in bidi contexts
    - `decomposition` (CharacterDecomposition): Codepoints to decompose to for
        varying degrees of normalization
    - `decimal_digit_value` (int): Numeric value of decimal digits
    - `digit_value` (int): Numeric value of digits
    - `numeric_value` (Fraction): Numeric value of other types of codepoints
        (e.g. ⅕ and other numeric non-digits)
    - `simple_uppercase` (int): Single-codepoint uppercase mapping
    - `simple_lowercase` (int): Single-codepoint lowercase mapping
    - `simple_titlecase` (int): Single-codepoint titlecase mapping
    - `special_casing` (list): Other case mappings (not necessarily single-
        codepoint) with possible conditions (position in word, language, etc.)
    - `folds` (dict): Case-folding mappings for case-
        insensitive comparison (generally lowercase mappings), both single-
        codepoint, multi-codepoint, and the special case of turkish case-
        foldings (e.g. I → ı)
    - `script` (string): Name of the primary script to which the codepoint
        belongs
    - `script_extensions` (list): Additional scripts which use the codepoint
    - `block` (string): The block to which the codepoint belongs
    - `line_break` (meta index): Line-breaking behaviour of the codepoint
    - `hangul_syllable_type` (meta index): The position and function of a
        hangul syllable
    - `unihan_variants` (list): Similar unihan codepoints of varying levels
        of compatibility; font variants, semantic variants, etc.
    - `unihan_numeric_type` (meta index): The type of numeric value represented
        by a unihan codepoint (e.g. kAccountingNumeric)
    - `unihan_numeric_value` (int): Numeric value of a unihan codepoint
    """
    def __init__(self, index, fields, meta):
        self.codepoint = index
        self.meta = meta

        self.handle_unicode_data(fields["UnicodeData"])
        self.handle_special_casing(fields["SpecialCasing"])
        self.handle_case_folding(fields["CaseFolding"])
        self.handle_script(fields["Scripts"])
        self.handle_script_extensions(fields["ScriptExtensions"])
        self.handle_block(fields["Blocks"])
        self.handle_line_break(fields["LineBreak"])
        self.handle_hangul_syllable_type(fields["HangulSyllableType"])
        self.handle_unihan_variants(fields["Unihan_Variants"])
        self.handle_unihan_numeric_values(fields["Unihan_NumericValues"])

    def handle_unicode_data(self, maybe_fields):
        """Load values from UnicodeData.txt entries

        Fields:
        0  Name
        1  General_Category
        2  Canonical_Combining_Class
        3  Bidi_Classs
        4  Decomposition_Type / Decomposition_Mapping
        5  Numeric_Value, Numeric_Type = Decimal
        6  Numeric_Value, Numeric_Type = Digit
        7  Numeric_Value, Numeric_Type = Numeric
        8  Bidi_Mirrored
        9  Unicode_1_Name (Obsolete)
        10 ISO_Comment (Obsolete)
        11 Simple_Uppercase_Mapping
        12 Simple_Lowercase_Mapping
        13 Simple_Titlecase_Mapping
        """
        if maybe_fields is None:
            return

        fields = list(maybe_fields)

        self.name = fields[0]
        self.category = self.meta.general_categories.get_index(fields[1])

        self.combining_class = int(fields[2], 10)
        self.bidi_class = self.meta.bidi_class.get_index(fields[3])
        self.bidi_mirrored = decode_boolean(fields[8])

        self.decomposition = None
        if fields[4]:
            self.decomposition = CharacterDecomposition(fields[4])

        self.decimal_digit_value = int(fields[5], 10) if fields[5] else None
        self.digit_value = int(fields[6], 10) if fields[6] else None
        self.numeric_value = Fraction(fields[7]) if fields[7] else None

        self.simple_uppercase = decode_codepoint(fields[11])
        self.simple_lowercase = decode_codepoint(fields[12])
        self.simple_titlecase = decode_codepoint(fields[13])

    def handle_special_casing(self, maybe_entries):
        """Load values from SpecialCasing.txt entries.

        Fields:
        0 Lower-case
        1 Title-case
        2 Upper-case
        3 Condition
        """
        self.special_casing = []

        if maybe_entries is None:
            return

        for entry in maybe_entries:
            fields = list(entry)

            self.special_casing.append({
                "lower": decode_codepoint_sequence(fields[0]),
                "title": decode_codepoint_sequence(fields[1]),
                "upper": decode_codepoint_sequence(fields[2]),
                "condition": fields[3],
            })

    def handle_case_folding(self, maybe_entries):
        """Load values from CaseFolding.txt entries

        A codepoint can have multiple case folding entries. Simple folds are
        one-to-one mappings, while full mappings will increase the length of
        the string. Turkish mappings are for handling dotted and dotless I's
        in the Turkish language.

        Fields:
        0 Status (C, F, S, T)
        1 Mapping
        """
        self.folds = {
            "simple": None,
            "full": None,
            "turkish": None,
        }

        if maybe_entries is None:
            return

        for entry in maybe_entries:
            fields = list(entry)

            status = fields[0]
            mapping = decode_codepoint_sequence(fields[1])

            if status == "C":
                self.folds["simple"] = mapping
                self.folds["full"] = mapping
            elif status == "S":
                self.folds["simple"] = mapping
            elif status == "F":
                self.folds["full"] = mapping
            elif status == "T":
                self.folds["turkish"] = mapping
            else:
                raise ValueError("Unknown case mapping type {}".format(status))

    def handle_script(self, maybe_fields):
        """Load values from Scripts.txt entries

        Each entry has one "script", but there also script extensions which
        are loaded from ScriptExtensions.txt

        Fields:
        0 Script
        """
        self.script = None

        if maybe_fields is None:
            return

        fields = list(maybe_fields)

        self.script = self.meta.scripts.get_index(fields[0])

    def handle_script_extensions(self, maybe_fields):
        """Load values from ScriptExtensions.txt

        Indicates characters which may be used in more than one script

        Fields:
        0 Scripts (space-delimited, short names)
        """
        self.script_extensions = []

        if maybe_fields is None:
            return

        fields = list(maybe_fields)

        for script in fields[0].split():
            self.script_extensions.append(
                self.meta.scripts.get_index(script))

    def handle_block(self, maybe_fields):
        """Load values from Blocks.txt

        Fields:
        0 Block
        """
        self.block = None

        if maybe_fields is None:
            return

        fields = list(maybe_fields)
        self.block = self.meta.blocks.get_index(fields[0])

    def handle_line_break(self, maybe_fields):
        """Load values from LineBreak.txt

        Indicates the line breaking properties of a codepoint; e.g., if
        a character creates a line break, a line break opportunity, or
        prohibits line breaks.

        According to UAX14, the default value for codepoints should be XX;
        except in some CJK blocks, planes 2 and 3, and U+1F000..U+1FFFD,
        in which case it should be ID. Also, unassigned codepoints in the
        currency symbols block should default to PR.

        Fields:
        0 Line_Break
        """
        self.line_break = self.meta.line_breaks.get_index("XX")

        in_id_block = self.block in self.meta.line_break_id_blocks
        in_id_range = any(
            (self.codepoint in r for r in self.meta.line_break_id_ranges))
        in_pr_block = self.block == self.meta.line_break_pr_block

        if in_id_block or in_id_range:
            self.line_break = self.meta.line_breaks.get_index("ID")

        if in_pr_block:
            self.line_break = self.meta.line_breaks.get_index("PR")

        if maybe_fields is None:
            return

        fields = list(maybe_fields)

        self.line_break = self.meta.line_breaks.get_index(fields[0])

    def handle_hangul_syllable_type(self, maybe_fields):
        """Load values from HangulSyllableTypes.txt

        Syllable types affect joining and normalization between consonant
        codepoints and syllable codepoints

        Fields:
        0 Hangul_Syllable_Type
        """
        self.hangul_syllable_type = None

        if maybe_fields is None:
            return

        fields = list(maybe_fields)

        self.hangul_syllable_type = self.meta.hangul_syllable_types.get_index(
            fields[0])

    def handle_unihan_variants(self, maybe_entries):
        """Load values from Unihan_Variants.txt

        Each codepoint may have several types of variants, which may each
        have several candidates. Semantic variants are accompanied by a
        dictionary source

        Fields:
        0    Variant_Type
        1..n Variant (e.g. U+4FDE<kMatthews,kMeyerWempe)
        """
        self.unihan_variants = None

        if maybe_entries is None:
            return

        self.unihan_variants = []
        for entry in maybe_entries:
            fields = list(entry)
            self.unihan_variants.append(UnihanVariant(fields, self.meta))

    def handle_unihan_numeric_values(self, maybe_fields):
        """Load values from Unihan_NumericValues.txt

        Many characters can represent a word or number. Some characters are
        used in accounting contexts to prevent forgery (e.g. 十 can be easily
        changed to 千, so 拾 may be used instead)

        Fields:
        0 Type (kOtherNumeric, kPrimaryNumeric, kAccountingNumeric)
        1 Value
        """
        self.unihan_numeric_type = None
        self.unihan_numeric_value = None

        if maybe_fields is None:
            return

        fields = list(maybe_fields)

        self.unihan_numeric_type = self.meta.unihan_numeric_values.get_index(
            fields[0])
        self.unihan_numeric_value = int(fields[1])

    def __str__(self):
        lines = ["0x{:X}".format(self.codepoint)]

        fmt = " » {} ⇒  {}"

        def add(prop):
            """Add property name and value to output"""
            value = getattr(self, prop, None)

            val = value
            if isinstance(val, list):
                val = list(map(str, val))

            lines.append(fmt.format(prop, val))

        add("name")
        add("category")
        add("decomposition")
        add("decimal_digit_value")
        add("folds")
        add("special_casing")
        add("script")
        add("script_extensions")
        add("line_break")
        add("hangul_syllable_type")
        add("unihan_variants")
        add("unihan_numeric_type")
        add("unihan_numeric_value")

        return "\n".join(lines)
