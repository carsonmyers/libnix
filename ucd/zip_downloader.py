"""Utility to download unicode database updates
"""
import random
import string
import zipfile
from ftplib import FTP
from os import path, makedirs, remove, scandir, rmdir
from urllib import request
from urllib.parse import urlparse, urljoin

from ucd.errors import (
    InvalidDownloadPathError,
    DuplicateTargetError,
    UnsupportedUrlSchemeError,
    FTPNetworkError
)

class ZipDownloader:
    """Downloader for UCD zip archives

    `source` can be an HTTPS, or FTP URL, and `target_paths` are paths to zip
    archives (relative to `source`) which will be downloaded.
    """
    def __init__(self, source, *target_paths):
        self.__source = urlparse(source)
        self.__archives = []
        slug = ZipDownloader.__make_slug()

        self.__targets = {}
        for target in target_paths:
            basename, ext = path.splitext(path.basename(target.lower()))

            # Only zip archives are currently supported (since that's what
            # unicode.org distributes)
            if ext != ".zip":
                raise InvalidDownloadPathError(target)

            # Don't specify multiple targets of the same name (they will all
            # be downloaded to the same destination)
            if basename in self.__targets:
                raise DuplicateTargetError(
                    basename, target, self.__targets[basename])

            # Only `target_path` and `arc_name` are functionally necessary,
            # `target_full_path` is just used for error messages
            self.__targets[basename] = {
                "target_path": urljoin(self.__source.path + "/", target),
                "target_full_path": urljoin(
                    self.__source.geturl() + "/", target),
                "arc_name": "{}_{}.zip".format(basename, slug),
            }

    def download(self, dest):
        """Perform an HTTPS or FTP download

        `dest` is the local target directory to save the archives to.

        The archives will contain a random slug in case previous downloads
        were not cleaned up.
        """
        if not path.isdir(dest):
            makedirs(dest)

        if self.__source.scheme == "https":
            return self.__download_https(dest)
        elif self.__source.scheme == "ftp":
            return self.__download_ftp(dest)
        else:
            raise UnsupportedUrlSchemeError(
                self.__source.scheme, self.__source.geturl())

    def __download_https(self, dest):
        for info in self.__targets.values():
            local_zip = path.join(dest, info["arc_name"])
            info["arc_path"] = local_zip
            self.__archives.append(local_zip)

            request.urlretrieve(info["full_path"], local_zip)

    def __download_ftp(self, dest):
        with FTP(self.__source.netloc) as ftp:
            ZipDownloader.__ensure_ftp_success(
                ftp.login(), self.__source.netloc)

            for info in self.__targets.values():
                local_zip = path.join(dest, info["arc_name"])
                self.__archives.append(local_zip)

                command = "RETR {}".format(info["target_path"])
                with open(local_zip, "wb") as outp:
                    result = ftp.retrbinary(command, outp.write)

                ZipDownloader.__ensure_ftp_success(
                    result, info["target_full_path"])

    def extract(self, dest):
        """Extract downloaded archives

        `dest` is the local target directory to save the extracted files to.

        All contained files will be extracted to the same place - archives
        should together contain exactly one copy of the unicode character
        database, without overlap.
        """
        if not path.isdir(dest):
            makedirs(dest)

        # Always start with a clear database
        ZipDownloader.__empty_database(dest)

        for archive in self.__archives:
            with zipfile.ZipFile(archive) as zip_file:
                zip_file.extractall(dest)

    def cleanup(self):
        """Clean up the downloaded archives

        There's nothing preventing this from being called before the archives
        have been extracted
        """
        for archive in self.__archives:
            remove(archive)

        self.__archives = []

    @staticmethod
    def __make_slug():
        source = string.ascii_letters + string.digits
        length = 10
        return "".join(random.choices(source, k=length))

    @staticmethod
    def __ensure_ftp_success(result, url):
        if result[0] != "2":
            raise FTPNetworkError(url)

    @staticmethod
    def __empty_database(dest):
        with scandir(dest) as entries:
            for entry in entries:
                if entry.is_file():
                    remove(entry.path)
                elif entry.is_dir():
                    ZipDownloader.__empty_database(entry.path)
                    rmdir(entry.path)
