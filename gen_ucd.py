#!/usr/bin/env python3
from ucd.arguments import parse_args
from ucd.codepoint import Codepoint
from ucd.collator import Collator
from ucd.entry_reader import (
    UcdEntryReaderOptions,
    UnihanEntryReaderOptions,
    AliasEntryReaderOptions,
)
from ucd.file_reader import FileReader
from ucd.meta import Meta
from ucd.zip_downloader import ZipDownloader
import itertools
import sys


def main():
    args = parse_args(sys.argv[1:])

    if args.test:
        test_file_reader()
        return

    if args.update_ucd:
        downloader = ZipDownloader(args.ucd_source, *args.ucd_archives)
        downloader.download(args.ucd_archive_destination)
        downloader.extract(args.ucd_data_destination)
        downloader.cleanup()

    meta = Meta(get_meta())
    generate_meta_tables(meta)
    generate_unicode_tables(meta)


def generate_meta_tables(meta):
    pass


def generate_unicode_tables(meta):
    collator = get_ucd_collator()

    def instantiate_codepoint(c):
        try:
            return Codepoint(c["index"], c["fields"], meta)
        except:
            print("E 0x{:X}".format(c["index"]))
            for field, value in c["fields"].items():
                val = value
                if value is not None:
                    val = list(value)

                print(" » {} ⇒  {}".format(field, val))
            raise

    codepoints = map(instantiate_codepoint, collator)
    show(codepoints, 0x40, 20)


def get_ucd_collator():
    sources = [
        {
            "filename": "ucd/database/UnicodeData.txt",
            "options": UcdEntryReaderOptions(detect_pairs=True, name_field=1),
        },
        {
            "filename": "ucd/database/Unihan_Variants.txt",
            "options": UnihanEntryReaderOptions(),
        },
        {
            "filename": "ucd/database/Unihan_NumericValues.txt",
            "options": UnihanEntryReaderOptions(allow_multiples=False),
        },
        {
            "filename": "ucd/database/HangulSyllableType.txt",
            "options": UcdEntryReaderOptions(),
        },
        {
            "filename": "ucd/database/Blocks.txt",
            "options": UcdEntryReaderOptions(),
        },
        {
            "filename": "ucd/database/SpecialCasing.txt",
            "options": UcdEntryReaderOptions(
                allow_multiples=True, out_of_order=True),
        },
        {
            "filename": "ucd/database/CaseFolding.txt",
            "options": UcdEntryReaderOptions(
                allow_multiples=True, out_of_order=True)
        },
        {
            "filename": "ucd/database/LineBreak.txt",
            "options": UcdEntryReaderOptions(),
        },
        {
            "filename": "ucd/database/Scripts.txt",
            "options": UcdEntryReaderOptions(out_of_order=True),
        },
        {
            "filename": "ucd/database/ScriptExtensions.txt",
            "options": UcdEntryReaderOptions(out_of_order=True),
        }
    ]

    return Collator(*sources)


def get_meta():
    options = AliasEntryReaderOptions()

    alias_source = "ucd/database/PropertyAliases.txt"
    alias_reader = FileReader(alias_source, options)

    aliases = {}
    for entry in alias_reader:
        abbr_name = entry["index"]
        long_names = list(entry["fields"][0])
        aliases[abbr_name] = long_names

    value_alias_source = "ucd/database/PropertyValueAliases.txt"
    value_alias_reader = FileReader(value_alias_source, options)

    value_aliases = {}
    for entry in value_alias_reader:
        cat_name = entry["index"]
        long_names = aliases[cat_name]
        values = list(map(lambda items: list(items), entry["fields"]))

        value_aliases[cat_name] = values
        for long_name in long_names:
            value_aliases[long_name] = values

    return value_aliases


def test_file_reader():
    sources = [
        {
            "filename": "ucd/test/Test1.txt",
            "options": UcdEntryReaderOptions(),
        },
        {
            "filename": "ucd/test/Test2.txt",
            "options": UcdEntryReaderOptions(detect_pairs=True, name_field=1),
        },
        {
            "filename": "ucd/test/UnihanTest.txt",
            "options": UnihanEntryReaderOptions(),
        },
        {
            "filename": "ucd/test/OutOfOrder.txt",
            "options": UcdEntryReaderOptions(
                allow_multiples=True, out_of_order=True),
        },
    ]

    collator = Collator(*sources)

    for codepoint in collator:
        print_codepoint(codepoint)

    aliases = FileReader("ucd/test/Aliases.txt", AliasEntryReaderOptions())


def print_codepoint(codepoint):
    if isinstance(codepoint, Codepoint):
        line = "{}\n".format(str(codepoint))
        sys.stdout.buffer.write(line.encode('utf-8'))
        return

    print("0x{:X}:".format(codepoint["index"]))
    for source, values in codepoint["fields"].items():
        v = values
        if v is not None:
            v = list(v)

        line = " » {} ⇒  {}\n".format(source, v)
        sys.stdout.buffer.write(line.encode('utf-8'))


def show(iter, start, count):
    line = "\nshowing 0x{:X} → 0x{:X}\n".format(start, start + count)
    sys.stdout.buffer.write(line.encode('utf-8'))

    if start > 0:
        while True:
            val = next(iter)
            if val.codepoint == start - 1:
                break

    for _ in range(count):
        val = next(iter)
        print_codepoint(val)


if __name__ == "__main__":
    main()
